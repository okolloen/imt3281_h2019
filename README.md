# IMT3281_h2019

All kildekode fra forelesningene og løsningsforslag til mange av lab oppgavene vil bli lagt inn i dette repositoriet.

## forelesning

Denne katalogen vil inneholde kjernestoff fra forelesningene, temaene fra BlackBoard finnes for hver uke i denne katalogen

## lab

Denne katalogen inneholder utgangspunkt for lab oppgaver og løsningsforslag for lab oppgaver.

## extras

Noen eksempler passer ikke helt inn i forelesningsrekken men illustrerer teknologi som dere bør kjenne til, disse blir plassert i denne katalogen og referert til fra BlackBoard.
