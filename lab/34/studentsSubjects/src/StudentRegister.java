import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class StudentRegister {
    List<Student> students = new LinkedList<>();
    HashMap<Course, Integer> studentsInCourses = new HashMap<>();

    public StudentRegister() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Navn på studenten som skal registreres:");
        String name = scanner.nextLine();
        do {
            System.out.println("Hvilken klasse går denne studenten i?");
            String group = scanner.nextLine();
            Student student = new Student(name, group, scanner);
            students.add(student);
            System.out.println("Navn på studenten som skal registreres ('ingen flere studenter' dersom du er ferdig):");
            name = scanner.nextLine();
        } while (!name.equalsIgnoreCase("ingen flere studenter")); // Read all students
        scanner.close();

        for (Student student : students) { // Go trough all students
            for (Course course : student.getCourses()) { // and all their courses
                if (studentsInCourses.containsKey(course)) { // If we have already found this course
                    studentsInCourses.put(course, studentsInCourses.get(course) + 1); // add one student to the course
                } else {
                    studentsInCourses.put(course, 1); // if no students are registered for this course yet, set number
                                                      // of students to 1
                }
            }
        }

        System.out.println("Emner:");
        for (Course key : studentsInCourses.keySet()) { // Loop over keys in hashmap, print out information about all
                                                        // courses
            System.out.printf("I %s er det registrert %d studenter\n", key, studentsInCourses.get(key));
        }
        System.out.println("\nRegistrerte studenter:");
        for (Student student : students) { // Loop over students, write out student information
            System.out.println(student);
        }
    }

    public static void main(String[] args) {
        new StudentRegister();
    }
}