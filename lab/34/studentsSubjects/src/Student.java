import java.util.List;
import java.util.Scanner;
import java.util.LinkedList;

public class Student {
    private String name;
    private String group;
    private List<Course> courses = new LinkedList<>();

    public Student(String name, String group, Scanner scanner) {
        this.name = name;
        this.group = group;
        registrerEmner(scanner);
    }

    private void registrerEmner(Scanner scanner) {
        System.out.println("Emnekode for emne som denne student følger:");
        String courseCode = scanner.nextLine();
        do {
            System.out.println("Emnenavn:");
            String courseName = scanner.nextLine();
            courses.add(new Course(courseCode, courseName));
            System.out.println(
                    "Emnekode for emne som denne student følger ('ingen flere emner' dersom du er ferdig med å registrere emner for denne studenten.):");
            courseCode = scanner.nextLine();
        } while (!courseCode.equalsIgnoreCase("ingen flere emner"));
    }

    public List<Course> getCourses() {
        return courses;
    }

    public String toString() {
        return name + " går " + group;
    }
}