public class Course {
    private String code, name;

    public Course(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return code + " " + name;
    }

    @Override
    public boolean equals(Object o) {
      return ((o instanceof Course)&&
              ((Course) o).code.equals(code));
    }

    @Override
    public int hashCode() { // Need both this and equals for the containsKey in HashMap to work correctly
        return code.hashCode();
    }
}
