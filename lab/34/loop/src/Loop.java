public class Loop {

    String two = "", three = "", four = "";

    private String append(String existing, String toAppend) {
        if (existing.length() > 0) { // We already have something
            existing += ", "; // add ", " as separator between items
        }
        return existing += toAppend;
    }

    public Loop() { // Do the work in the constructor (if not, everything would have had to been
                    // declared as static)
        for (int i = 1; i <= 10; i++) {
            if (i % 2 == 0) {
                two = append(two, Integer.toString(i));
            }
            if (i % 3 == 0) {
                three = append(three, Integer.toString(i));
            }
            if (i % 4 == 0) {
                four = append(four, Integer.toString(i));
            }
        }
        System.out.printf("%s\n%s\n%s\n", two, three, four);
    }

    public static void main(String[] args) {
        new Loop();
    }
}