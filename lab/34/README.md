# Lab/øvingsoppgaver for uke 34

For de som ikke har et Java utviklingsmiljø på sin maskin, last ned og installer Java Development Kit, link til nedlasting finnes [for standard JDK](https://www.oracle.com/technetwork/java/javase/overview/index.html) eller [for openJDK](https://jdk.java.net/). (Når dette skriver, 2. august 2019, er det JDK 12 som er gjeldende versjon).

------------

Skriv et enkelt "Hello world" program, kompiler og kjør dette fra kommandolinja (fra og med neste uke forlater vi kommandolinja men det er greit å sjekke at ting virker.)

------------

Lag et program som har en løkke som går fra 1 til 10, skriv ut alle tall som kan deles på 2, 3 og 4, formater utskriften slik :

  Tall som kan deles på 2 : 2, 4, 6, 8, 10
  Tall som kan deles på 3 : 3, 6, 9
  Tall som kan deles på 4 : 4, 8

------------

Lag et program som leser inn data fra brukeren, les inn kjørte km og drivstoff forbruk og skriv ut forbruket pr. mil. Utvid så programmet slik at det fortsetter å lese inn frem til brukeren skriver "q" og trykker enter. For hver gang brukeren har skrevet inn kjørte km og forbruk så skrives det ut forbruk pr. mil for siste registrering og gjennomsnittsforbruk.

------------

Lag et program for å registrere studenter og emner, programmet bruker objekter av klassen Student for å holde informasjon om studenter. Inne i Student finnes navn og klasse og en liste med Emne objekter, inne i hvert Emne objekt finnes emnekode og emnenavn. Når programmet starter så skal det lese inn navn og klasse for en student, deretter opprettes emne objekter inne i student objektet frem til brukeren skriver en linje med "ingen flere emner" og enter. Da kan brukeren enten skrive "ingen flere studenter", evnt navn på en ny student.

Lag til slutt en rapport med oversikt over alle emner og studenter som er registrert (hvert emne skal skrives ut en gang, med antall studenter som er registrert.) Lag en fil med informasjon om studenter og emner som du kan pipe inn til programmet, det blir fort kjedelig å skrive inn informasjonen hver gang du har gjort en endring for å teste om det virker.

-----------

Lag et tomt (uten metodedefinisjoner) interface "Shape", lag så et interface "Circle" med metode "public void getRadius(Scanner scanner);" som arver fra Shape. Lag også interfacet "Square" med metoden "public void getDimensions(Scanner scanner);" som også arver fra "Shape". Lag så et program som kan lese inn sirkler og firkanter fra brukeren (eller via en pipe fra en fil.)

Les inn "sirkel" eller "firkant" fra brukeren, deretter oppretter du et objekt av Circle eller Square og bruker metoden "getRadius" eller "getDimensions" for å finne størrelsen til objektet. Legg til en "public String toString();" metode i objektene så vi enkelt kan skrive de ut til slutt.

Legg til hvert objekt i en liste (LinkedList) som inneholder "Shape" objekter.

Fortsett å lese inn sirkler/firkanter til du kommer til en linje med ordet "slutt".

Når alt er lest inn går du gjennom listen og skriver ut alle objektene.
