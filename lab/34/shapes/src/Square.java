import java.util.Scanner;

public interface Square extends Shape {
    public void getDimensions(Scanner scanner);
}