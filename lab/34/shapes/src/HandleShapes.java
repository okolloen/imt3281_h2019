import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class HandleShapes {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Shape> shapes = new LinkedList<Shape>(); // A list containing Shape objects
        String tmp = scanner.next();
        do {
            if (tmp.equalsIgnoreCase("sirkel")) {
                Shape s = new Circle() { // Create a new Shape object, using the Circle interface. Must define getRadius
                    int r; // Store the radius

                    public void getRadius(Scanner scanner) {
                        r = scanner.nextInt(); // Get radius from scanner
                    }

                    @Override // Override, this is defined in the Object class
                    public String toString() { // toString method is called when converting an object to a String
                        return "Sirkel med radius r: " + r;
                    }
                };
                ((Circle) s).getRadius(scanner); // Use the getRadius method to get the radius
                shapes.add(s); // Add this Shape object to the list og shapes
            } else if (tmp.equalsIgnoreCase("firkant")) {
                Shape s = new Square() { // Create a new Shape object, using the Square interface. Must define
                                         // getDimensions
                    int h, w; // Store height and width

                    public void getDimensions(Scanner scanner) {
                        h = scanner.nextInt(); // Get height and width from scanner
                        w = scanner.nextInt();
                    }

                    @Override
                    public String toString() {
                        return "Firkant med høyde: " + h + " og bredde: " + w;
                    }
                };
                ((Square) s).getDimensions(scanner); // Use getDimensions to get height and width from scanner
                shapes.add(s);
            }
            scanner.nextLine(); // Fjerner linjeskiftet
        } while (!(tmp = scanner.next()).equalsIgnoreCase("slutt"));
        scanner.close();
        for (Shape s : shapes) { // Loop over all shapes,
            System.out.println(s); // Print out each shape
        }
    }
}