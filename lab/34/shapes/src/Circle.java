import java.util.Scanner;

public interface Circle extends Shape {
    public void getRadius(Scanner scanner);
}