import java.util.Scanner;

public class FuelConsumption {
    int km = 0, liters = 0; // Used for calculating average consumption

    public FuelConsumption() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Hvor mange Km har du kjørt?");
        String input = scanner.nextLine();
        do {
            int driven = Integer.parseInt(input);
            System.out.println("Hvor mye drivstoff har du brukt (liter)?");
            int used = Integer.parseInt(scanner.nextLine());
            double usage = used * 1.0 / driven * 10.0; // Consumption on last enrty
            System.out.printf("Du har brukt %.2f l/mil på å kjøre %d km med et forbruk på %d liter.\n", usage, driven,
                    used);
            km += driven; // Sum up km driven
            liters += used; // and liters used
            usage = liters * 1.0 / km * 10.0; // to be able to calculate average
            System.out.printf(
                    "Gjennomsnittsforbruket ditt er %.2f l/mil etter å ha kjørt %d km med et forbruk på %d liter.\n",
                    usage, km, liters);
            System.out.println("Hvor mange Km har du kjørt (q for å avslutte)?");
            input = scanner.nextLine();
        } while (!input.equals("q"));
        scanner.close();
    }

    public static void main(String[] args) {
        new FuelConsumption();
    }
}