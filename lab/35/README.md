# Lab/øvingsoppgaver for uke 35

Opprett et nytt prosjekt som et maven prosjekt (uten bruk av archetype). Lag så et nytt git repository (github, bitbucket, ....). Lag en .gitignore fil slik at kun de nødvendige filene blir lagret i git. Koble git repositoriet til den lokale katalogen og last opp prosjektet.

Klon dette prosjektet til en ny katalog, sjekk at du kan åpne og kjøre prosjektet. Prøv gjerne med ulike IDE'er. Dersom en lager et prosjekt i IntelliJ/Eclipse/.... uten å bruke maven (eller liknende) så vil en ikke enkelt kunne bruke samme prosjektet i en annen IDE. I store prosjekter vil det alltid bli brukt et slikt konfigurasjonsstyringsverktøy.

[Demo repository](https://bitbucket.org/okolloen/imt3281_demo_h2019/src/master/)

--------------------------

Lag et nytt prosjekt, denne gangen et maven/JavaFX prosjekt. Sjekk at du får tilgang til maven/plugins/javafx/compile og run.

Erstatt innholdet i primary.fxml med et bilde (valgfritt bilde). Fjern secondary.fxml og endre koden i App.java slik at den er optimal (uten noen spor av det som skal til for å bytte mellom to ulike fxml filer.)

--------------------------

Lag et nytt prosjekt som over, men denne gangen skal det lages et grensesnitt som for en e-post klient. Den vil naturlig ha en meny øverst med en toolbar under. Til venstre ønsker vi å ha en tree-view for å vise inboksen og ulike foldere under som en kan sortere e-posten i. I midten av skjermbildet så ønsker en å ha en oversikt over alle e-poster i valgt folder, dette kan f.eks. være en liste. Til høyre så er det naturlig å vise innholdet av valgt e-post. Bruk ulike layout typer som du mener passer.
