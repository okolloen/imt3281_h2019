module no.ntnu.imt3281.lab {
    requires javafx.controls;
    requires javafx.fxml;

    opens no.ntnu.imt3281.lab to javafx.fxml;
    exports no.ntnu.imt3281.lab;
}