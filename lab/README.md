## Utgangspunkt for lab oppgaver og løsningsforslag

|  Uke  | Tema                    |
| :---: | ----------------------- |
| 34    | Introduksjon til Java   |
| 35  | Lage prosjekter, både med og uten JavaFX med Maven. Lage et grensesnitt i JavaFX. |
| 36 | Enkel håndtering av hendelser i GUI. Dokumentasjon med JavaDoc, I18N |
| 37 | Arrayer, lister, collections rammeverket. Generiske typer. Exception håndtering, filer |
| 38 | Testing med JUnit. Statisk kodetesting med SonarQube. Logging |
| 39 | Mer om JavaFX, kapittel 12, 13 og 22 i læreboka |
| 40 | Nettverk, flertrådsprogrammering, lambda og streams |
| 41 | Databaser |