Lag en klasse som fungerer som en sortert liste. Den beste måten å gjøre dette på er å lage en klasse som implementerer java.util.List interfacet. Bruk generics, dvs la brukeren lagre en vilkårlig type (objekter av en vilkårlig klasse) som implementerer Comparable interfacet.

Inne i klassen kan dere bruke en ArrayList eller en LinkedList for å lagre dataene. De fleste av funksjonene fra java.util.list kan dere implementere ved å kalle de tilsvarende funksjonene på listen dere bruker for å lagre data.

håndter add(T value) spesielt, denne skal sette inn verdien på den posisjonen i listen som sørger for at listen fortsetter å være sortert. Bruk java.util.Collections.binarySearch for å finne ut hvor elementet skal plasseres.

---------------------

Bruk opp igjen Student klassen fra forrige uke, men denne gangen skal (filen med studenter)[students.txt] leses inn ved hjelp av BufferedReader. Legg til Serializable interfacet på Studentklassen og skriv ut igjen alle studenter til en fil med ObjectOutputStream.

---------------------

Legg til Comparable interfacet på klassen Student og implementer Compare metoden, vi ønsker å sortere på navnet på studenten.

Les inn fra Filen du skrev i oppgaven over og legg inn den sorterte listen du lagde i første oppgaven. Skriv nå ut listen med studentene sortert på navn.

---------------------

For å kunne lese inn Objekter må objektene som er skrevet til fil ha samme klasseversjon som den klassen som finnes i systemet. Denne versjonen genereres automatisk når klassen kompileres men kan overstyres med egenskapen :


`private static final long serialVersionUID = versjonsnummerL;`

------------------------

Lag et program som når det starter først prøver å lese en properties fil fra katalogen programmet startes fra. Dersom denne filen ikke finnes prøver en å lese samme properties fil fra "resources" (du må lage denne filen i resources mappen.)

Når programmet avsluttes skal inneholdet i Properties objektet skrives til properties filen i katalogen programmet ble startet fra (slik at eventuelt nye/endrede egenskaper lastes inn neste gang programmet startes.)

Egenskapene i properties filen i "resources" mappen brukes altså kun som default verdier første gang programmet startes.
