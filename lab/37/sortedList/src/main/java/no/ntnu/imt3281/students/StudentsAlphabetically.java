package no.ntnu.imt3281.students;

import no.ntnu.imt3281.util.SortedArrayList;

import java.io.*;

public class StudentsAlphabetically {
    SortedArrayList<Student> students = new SortedArrayList<>();
    public StudentsAlphabetically() {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("students.obj"))) {
            Student student;
            while ((student = (Student) ois.readObject()) != null) {
                students.add(student);
            }
        } catch (EOFException e) {
            // EOF is OK
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        for (Student s : students) {
            System.out.println(s);
        }
    }

    public static void main(String[] args) {
        new StudentsAlphabetically();
    }

}
