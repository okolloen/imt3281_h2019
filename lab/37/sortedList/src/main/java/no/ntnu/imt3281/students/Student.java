package no.ntnu.imt3281.students;

import java.io.Serializable;

/**
 * Objects of this class holds information about a single student
 */
public class Student implements Serializable, Comparable<Student> {
    private static final long serialVersionUID = 1L;
    String givenName, familyName;
    String group;

    /**
     * Create a new student with the give information
     *
     * @param givenName the first name of the student
     * @param familyName the last name of the student
     * @param group which group (class) does the student belong to
     */
    public Student (String givenName, String familyName, String group) {
        this.givenName = givenName;
        this.familyName = familyName;
        this.group = group;
    }

    /**
     * A string containing information about a student, given name, last name and group should be separated by ";"
     *
     * @param csv the semi-colon separated string containing information about the student
     */
    public Student (String csv) {
        String[] parts = csv.split(";");
        String tmp = parts[2];
        String group[] = tmp.split("\\)");
        this.givenName = parts[0];
        this.familyName = parts[1];
        this.group = group[0]+")";
    }

    /**
     * Get the students first (given) name.
     *
     * @return the given name of the student
     */
    public String getGivenName() {
        return givenName;
    }

    /**
     * Get the students last (family) name.
     *
     * @return the family name of the student
     */
    public String getFamilyName() {
        return familyName;
    }

    /**
     * Get the students group (class)
     *
     * @return the group of the student
     */
    public String getGroup() {
        return group;
    }

    /**
     * @see Object#toString()
     * @return information about the student
     */
    @Override
    public String toString() {
        return givenName+" "+familyName+" - "+group;
    }

    /**
     * Compares this object with the specified object for order.  Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.
     *
     * @param o the object to be compared.
     * @return a negative integer, zero, or a positive integer as this object
     * is less than, equal to, or greater than the specified object.
     * @throws NullPointerException if the specified object is null
     * @throws ClassCastException   if the specified object's type prevents it
     *                              from being compared to this object.
     */
    @Override
    public int compareTo(Student o) {
        if (o==null) throw new NullPointerException("Unable to compare to null");
        return toString().compareTo(o.toString());
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Student&&
                compareTo((Student)o)==0);
    }
}
