package no.ntnu.imt3281.tester;

import no.ntnu.imt3281.util.SortedArrayList;

public class SortedListTester {
    public static void main(String[] args) {
        SortedArrayList<String> tmp = new SortedArrayList<>();
        tmp.add("C");
        tmp.add("A");
        tmp.add("D");
        tmp.add("B");
        System.out.println(tmp);

    }
}
