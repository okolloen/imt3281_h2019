package no.ntnu.imt3281.students;

import java.io.*;

public class Convert2Objects {
    public Convert2Objects() {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/students.txt")));
             ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("students.obj"))) {
            String tmp = br.readLine();
            int numberOfStudents = 0;
            while ((tmp = br.readLine())!=null) {
                Student student = new Student(tmp);
                oos.writeObject(student);
                numberOfStudents++;
            }
            System.out.printf("%d students written to 'students.obj'", numberOfStudents);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new Convert2Objects();
    }
}
