/**
 * Objects of this class holds information about a single student
 */
public class Student {
    String givenName, familyName;
    String group;

    /**
     * Create a new student with the give information
     *
     * @param givenName the first name of the student
     * @param familyName the last name of the student
     * @param group which group (class) does the student belong to
     */
    public Student (String givenName, String familyName, String group) {
        this.givenName = givenName;
        this.familyName = familyName;
        this.group = group;
    }

    /**
     * A string containing information about a student, given name, last name and group should be separated by ";"
     *
     * @param csv the semi-colon separated string containing information about the student
     */
    public Student (String csv) {
        String[] parts = csv.split(";");
        String tmp = parts[2];
        String group[] = tmp.split("\\)");
        this.givenName = parts[0];
        this.familyName = parts[1];
        this.group = group[0]+")";
    }

    /**
     * Get the students first (given) name.
     *
     * @return the given name of the student
     */
    public String getGivenName() {
        return givenName;
    }

    /**
     * Get the students last (family) name.
     *
     * @return the family name of the student
     */
    public String getFamilyName() {
        return familyName;
    }

    /**
     * Get the students group (class)
     *
     * @return the group of the student
     */
    public String getGroup() {
        return group;
    }

    /**
     * @see Object#toString()
     * @return information about the student
     */
    @Override
    public String toString() {
        return givenName+" "+familyName+" - "+group;
    }
}
