import java.text.ChoiceFormat;
import java.text.Format;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.*;

/**
 * Read information about the student from standard input (or get the information from a file (pipe it in))<br>
 * <img src="./doc-files/Students.png" alt="Screenshot showing how to pipe information into the application"><br>
 *
 */
public class Students {
    List<Student> students = new LinkedList<Student>();

    /**
     * Constructor, will read data from standard input and fill the list with students.
     * Calls the method klasseOversikt to create the report.
     *
     * @see #klasseOversikt()
     */
    public Students() {
        Scanner scanner = new Scanner(System.in);
        String tmp = scanner.nextLine();
        tmp = scanner.nextLine();
        do {
            students.add(new Student(tmp));
            tmp = scanner.nextLine();
        } while (tmp.length()>0);
        scanner.close();
        for (Student s : students) {
            System.out.println(s);
        }
        klasseOversikt();
    }

    /**
     * When the list of student have been filled this method will figure out how many students there are in each group
     * (class).
     *
     * Prints a report to std out.
     */
    private void klasseOversikt() {
        // Go trough all students and group them into classes (groups)
        HashMap<String, List<Student>> groups = new HashMap<String, List<Student>>();
        for (Student s : students) {
            if (groups.containsKey(s.getGroup())) {
                groups.get(s.getGroup()).add(s);
            } else {
                List<Student> studentsInGroup = new LinkedList<Student>();
                studentsInGroup.add(s);
                groups.put(s.getGroup(), studentsInGroup);
            }
        }

        // Get the i18n bundle for default locale
        ResourceBundle bundle = ResourceBundle.getBundle("bundle", Locale.getDefault());

        // Create a MessageFormater to handle plurals
        MessageFormat messageForm = new MessageFormat("");
        messageForm.setLocale(Locale.getDefault());

        double[] fileLimits = {0,1,2};

        String [] fileStrings = {
                bundle.getString("noStudents"),
                bundle.getString("oneStudent"),
                bundle.getString("multipleStudents")
        };

        ChoiceFormat choiceForm = new ChoiceFormat(fileLimits, fileStrings);

        // First we handle the number of students in total
        String pattern = bundle.getString("studentsTotal");
        Format[] formats = {choiceForm, null, NumberFormat.getInstance()};

        messageForm.applyPattern(pattern);
        messageForm.setFormats(formats);

        Object[] messageArguments = {students.size(), "", students.size()};
        System.out.printf(messageForm.format(messageArguments));

        // Now print out the number of students in each group (class)
        pattern = bundle.getString("studentFromGroup");

        messageForm.applyPattern(pattern);
        messageForm.setFormats(formats);
        for (String key : groups.keySet()) {
            messageArguments[0] = groups.get(key).size();
            messageArguments[1] = key;
            messageArguments[2] = groups.get(key).size();
            System.out.printf (messageForm.format(messageArguments));
        }
    }

    /**
     * Starts the application
     *
     * @param args not used, only here to satisfy the implementation specification.
     */
    public static void main(String[] args) {
        new Students();
    }
}
