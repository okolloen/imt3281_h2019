Lag et enkelt grensesnitt med et tekstfelt under et tekstområde. legg til en knapp til høyre for tekstfeltet. Til høyre for både tekstområdet og tekstfeltet (og knappen) legges noen radioknapper, radioknappene brukes for å velge hvordan teksten fra tekstfeltet skal legges over i tekstområdet.

Legg til radioknapper for å legge teksten over i tekstområdet :

- Uforandret
- Som store bokstaver
- Som små bokstaver
- Med teksten bak frem

Lag så funksjonalitet til programmet slik at når brukeren trykker på knappen eller trykker enter i tekstfeltet så flyttes teksten til tekstområdet (legges til som ny linje) i tekstområdet ut ifra hvilken radioknapp som er valgt.

![Skjermdump](https://bitbucket.org/okolloen/imt3281_h2019/downloads/Skjermbilde%202019-09-03%20kl.%2013.31.42.png)

---------------------

Dokumenter programmet ditt ved hjelp av JavaDoc og generer dokumentasjon.

---------------------

Ta utgangspunkt i programmet fra [forelesningen](https://bitbucket.org/okolloen/imt3281_h2019/src/master/forelesning/35/packages/src/main/java/no/ntnu/imt3281/pakke/) som gir en oversikt over studenter i en klasse. Dette programmet skal internasjonaliseres. Gi riktig resultat for tilfellene der det går 1 student i en klasse, eller det går x studenter i en klasse (hvor x>1).

Husk JavaDoc kommentarer.
