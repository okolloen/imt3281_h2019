module no.ntnu.imt3281.uke36 {
    requires javafx.controls;
    requires javafx.fxml;

    opens no.ntnu.imt3281.uke36 to javafx.fxml;
    exports no.ntnu.imt3281.uke36;
}