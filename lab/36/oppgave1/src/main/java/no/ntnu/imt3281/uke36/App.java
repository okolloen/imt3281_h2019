package no.ntnu.imt3281.uke36;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * JavaFX App, illustrerer enkel hendelseshåndtering og dokumentasjon i Java.
 */
public class App extends Application {

    private static Scene scene;

    /**
     * Metoden kalles når applikasjonen starter, brukes for å laste inn FXML filen og vise innholdet av denne på skjermen.
     *
     * @param stage en referanse til vinduet som er opprettet for denne applikasjonen.
     *
     * @see javafx.application.Application#start(Stage)
     *
     * @throws IOException dersom fxml filen ikke kan lastes
     */
    @Override
    public void start(Stage stage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("primary.fxml"));
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Denne metoden kjøres når programmet lastes inn, kaller {@link javafx.application.Application#launch(String...)} som starter opp JavaFX.
     *
     * @param args ikke brukt, men må være med for å tilfredsstille definisjonen av main metoden.
     */
    public static void main(String[] args) {
        launch();
    }

}