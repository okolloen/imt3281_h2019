package no.ntnu.imt3281.uke36;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * Controller klasse for primary.fxml. Inneholder all funksjonalitet for applikasjonen.
 * Kan overføre tekst fra et tekstfelt til et tekstområde uforandret, som små/store bokstaver eller reversert (baklengs.)
 */
public class PrimaryController {

    @FXML private RadioButton unchanged;
    @FXML private RadioButton toLowerCase;
    @FXML private RadioButton toUpperCase;
    @FXML private RadioButton backwards;
    @FXML private TextArea target;
    @FXML private TextField source;

    /**
     * Kalles når brukeren trykker på den eneste knappen i applikasjonen evnt. trykker enter i tekstfeltet.
     * Sjekker hvilken radioknapp som er trykket inn og overfører teksten i følge disse.
     *
     * @param event ikke brukt, men må være med pga implementasjonen.
     */
    @FXML
    void transferText(ActionEvent event) {
        if (unchanged.isSelected()) {
            transferUnchanged();
        } else if (toLowerCase.isSelected()) {
            transferAsLowerCase();
        } else if (toUpperCase.isSelected()) {
            transferAsUpperCase();
        } else if (backwards.isSelected()) {
            transferBackwards();
        }
    }

    /**
     * Overfører teksten fra tekstfeltet til tekstområdet uforandret.
     * Sletter teksten i tekstfeltet når teksten er overført.
     */
    private void transferUnchanged() {
        append (source.getText());
        source.setText("");
    }

    /**
     * Overfører teksten fra tekstfeltet til tekstområdet men gjør om til bare små bokstaver.
     * Sletter teksten i tekstfeltet når teksten er overført.
     */
    private void transferAsLowerCase() {
        append (source.getText().toLowerCase());
        source.setText("");
    }

    /**
     * Overfører teksten fra tekstfeltet til tekstområdet men gjør om til bare store bokstaver.
     * Sletter teksten i tekstfeltet når teksten er overført.
     */
    private void transferAsUpperCase() {
        append (source.getText().toUpperCase());
        source.setText("");
    }

    /**
     * Overfører teksten fra tekstfeltet til tekstområdet etter at teksten er reversert (baklengs).
     * Sletter teksten i tekstfeltet når teksten er overført.
     */
    private void transferBackwards() {
        StringBuilder sb = new StringBuilder(source.getText());
        append (sb.reverse().toString());
        source.setText("");
    }

    /**
     * Legger til teksten i parameteren til slutt i tekstområdet.
     *
     * @param text den teksten som skal legges til i tekstområdet.
     */
    private void append(String text) {
        target.setText(target.getText()+text+"\n");
    }
}
