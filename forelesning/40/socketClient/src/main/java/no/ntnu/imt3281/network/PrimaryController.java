package no.ntnu.imt3281.network;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.io.*;
import java.net.ConnectException;
import java.net.Socket;

public class PrimaryController {
    private static final int DEFAULT_PORT = 4000;
    @FXML private TextField server;
    @FXML private Button connectButton;
    @FXML private TextArea receivedText;
    @FXML private Button sendButton;
    @FXML private TextField textToSend;

    private Socket connection = null;
    private BufferedWriter bw;
    private BufferedReader br;
    private boolean connected = false;

    @FXML
    void initialize() {
        Thread t = new Thread() {
            public void run() {
                while (true) {
                    if (connected) {
                        try {
                            final String in = br.readLine();
                            Platform.runLater(()->{
                                receivedText.appendText(in+"\n");
                            });
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        };
        t.setDaemon(true);
        t.start();
    }
    @FXML
    void connect(ActionEvent event) {
        if (!connected) {
            connect();
        } else {
            disconnect();
        }
    }

    private void disconnect() {
        try {
            bw.close();
            br.close();
            connection.close();
            connected = false;
            connectButton.setText("Connect");
            sendButton.setDisable(true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void connect() {
        String serverName = server.getText();
        int port = DEFAULT_PORT;
        if (serverName.indexOf(":") > 0) {
            try {
                port = Integer.parseInt(serverName.split(":")[1]);
            } catch (NumberFormatException e) {
                receivedText.appendText("Use serverName:port\n");
                return;
            }
            serverName = serverName.split(":")[0];
        }
        try {
            connection = new Socket(serverName, port);
            bw = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
            br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            connected = true;
            connectButton.setText("Disconnect");
            sendButton.setDisable(false);
            receivedText.appendText("Connected to " + serverName + " on port " + port+"\n");
        } catch (ConnectException e) {
            receivedText.appendText("Kunne ikke koble til serveren.\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void send(ActionEvent event) {
        try {
            bw.write(textToSend.getText());
            bw.newLine();
            bw.flush();
            textToSend.setText("");
        } catch (IOException e) {
            e.printStackTrace();
            receivedText.appendText("Unable to send to server\n");
        }
    }
}
