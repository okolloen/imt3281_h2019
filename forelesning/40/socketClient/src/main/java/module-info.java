module no.ntnu.imt3281.network {
    requires javafx.controls;
    requires javafx.fxml;

    opens no.ntnu.imt3281.network to javafx.fxml;
    exports no.ntnu.imt3281.network;
}