package no.ntnu.imt3281.httpsreader;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class HTTPSReader {
    public static void main(String[] args) throws IOException {
        URL url = new URL("https://www.ntnu.no");
        HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()))){
            String input;
            while ((input=br.readLine())!=null) {
                System.out.println(input);
            }
        } catch (IOException e) {

        }
    }
}
