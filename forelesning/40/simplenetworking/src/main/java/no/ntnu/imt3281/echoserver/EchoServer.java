package no.ntnu.imt3281.echoserver;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class EchoServer {
    public static void main(String[] args) {
        boolean stopping = false;
        try (ServerSocket server = new ServerSocket(4567)) {
            while (!stopping) {
                Socket s = server.accept();
                System.out.printf ("Connected from %s\n", s.getRemoteSocketAddress().toString());
                try (BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream())); BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()))) {
                    String tmp;
                    while ((tmp = br.readLine())!=null) {
                        if (tmp.equals("!STOP!")) {
                            stopping = true;
                        }
                        System.out.println(tmp);
                        bw.write(tmp);
                        bw.newLine();
                        bw.flush();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
