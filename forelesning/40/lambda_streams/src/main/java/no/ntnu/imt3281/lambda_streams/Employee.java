package no.ntnu.imt3281.lambda_streams;

public class Employee {
    private String firstName;
    private String lastName;
    private double salary;
    private String department;

    /**
     * @param firstName
     * @param lastName
     * @param salary
     * @param department
     */
    public Employee(String firstName, String lastName, double salary, String department) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
        this.department = department;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return String.format("%s %s", getFirstName(), getLastName());
    }

    /**
     * @return the salary
     */
    public double getSalary() {
        return salary;
    }

    /**
     * @param salary the salary to set
     */
    public void setSalary(double salary) {
        this.salary = salary;
    }

    /**
     * @return the department
     */
    public String getDepartment() {
        return department;
    }

    /**
     * @param department the department to set
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    @Override
    public String toString() {
        return String.format("%-8s %-8s %8.2f  %s",
                getFirstName(), getLastName(), getSalary(), getDepartment());
    }
}
