package no.ntnu.imt3281.lambda_streams;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ProcessingEmployees {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Employee employees[] = {
                new Employee("Jason", "Red", 5000, "IT"),
                new Employee("Ashley", "Green", 7600, "IT"),
                new Employee("Matthew", "Indigo", 3387.5, "Sales"),
                new Employee("James", "Indigo", 4700.77, "Marketing"),
                new Employee("Luke", "Indigo", 6200, "IT"),
                new Employee("Jason", "Blue", 3200, "Sales"),
                new Employee("Wendy", "Brown", 4236.4, "Marketing")
        };

        List<Employee> list = Arrays.asList(employees);

        System.out.println("Liste over alle ansatte:");
        list.stream().forEach(System.out::println);
        in.nextLine();
        // ==============

        Predicate<Employee> fourToSixThousand =
                e -> e.getSalary()>=4000 && e.getSalary()<=6000;

        System.out.println("Ansatte som tjener mellom 4000 og 6000 : ");
        list.stream()
                .filter(fourToSixThousand)
                .sorted(Comparator.comparing(Employee::getSalary))
                .forEach(System.out::println);

        System.out.printf("Den første ansatte som tjener mellom 4000 og 6000 : \n%s\n",
                list.stream()
                        .filter(fourToSixThousand)
                        .findFirst()
                        .get());
        in.nextLine();
        //===============

        Function<Employee, String> byFirstName = Employee::getFirstName;
        Function<Employee, String> byLastName = Employee::getLastName;

        Comparator<Employee> lastThenFirst =
                Comparator.comparing(byLastName).thenComparing(byFirstName);

        System.out.println("Ansatte sortert først på etternavn, så på fornavn : ");
        list.stream()
                .sorted(lastThenFirst)
                .forEach(System.out::println);

        System.out.println("\nAnsatte sortert i omvendt rekkefølge: ");
        list.stream()
                .sorted(lastThenFirst.reversed())
                .forEach(System.out::println);
        in.nextLine();
        //===============

        System.out.println("Unike etternavn:");
        list.stream()
                .map(Employee::getLastName)
                .distinct()
                .sorted()
                .forEach(System.out::println);

        System.out.println("\nNavn på ansatte, sortert på etternavn, så fornavn: ");
        list.stream()
                .sorted(lastThenFirst)
                .map(Employee::getName)
                .forEach(System.out::println);
        in.nextLine();
        //===============
        System.out.println("Ansatte ordnet etter avdeling");
        Map<String, List<Employee>> groupedByDepartment =
                list.stream()
                        .collect(Collectors.groupingBy(Employee::getDepartment));

        groupedByDepartment.forEach((department, employeesInDepartment) -> {
            System.out.printf("\n%s\n", department);
            employeesInDepartment.forEach(employee->
                    System.out.printf("	%s\n", employee)
            );
        });
        in.nextLine();
        //===============
        System.out.println("Antall ansatte i hver avdeling");
        Map<String, Long> employeeCountByDepartment =
                list.stream()
                        .collect(Collectors.groupingBy(Employee::getDepartment,
                                Collectors.counting()));
        employeeCountByDepartment.forEach((department, count)-> {
            System.out.printf("%s har %d ansatte\n", department, count);
        });
        in.close();
    }
}