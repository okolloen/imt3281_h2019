package no.ntnu.imt3281.lambda_streams;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class IntroLambda {
    static String handleInts (Functional operation, int... args) {
        return operation.doSomething(args);
    }

    public static void main(String[] args) {
        int oneToNine[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

        System.out.println(handleInts(new AddInts(), 1, 2, 3, 4, 5, 6, 7, 8, 9));

        System.out.println(handleInts(new Functional() {
            @Override
            public String doSomething(int... args) {
                StringBuffer sb = new StringBuffer();
                sb.append("Tallene er : ");
                for (int arg : args) {
                    sb.append(arg);
                    sb.append(" ");
                }
                return sb.toString();
            }
        }, 1, 2, 3, 4, 5, 6, 7, 8, 9));

        System.out.println(handleInts((numbers)-> {
            return String.format("Du sendte med %d tall", numbers.length);
        }, oneToNine));

        System.out.println(handleInts(numbers-> {
            return String.format("Det største tallet er : %d",
                    IntStream.of(numbers).max().getAsInt());
        }, oneToNine));

        System.out.println(handleInts(numbers-> String.format("Tallene er : %s",
                IntStream.of(numbers)
                        .mapToObj(String::valueOf)
                        .collect(Collectors.joining(", "))), oneToNine));
    }
}

interface Functional {
    public String doSomething(int... args);
}

class AddInts implements Functional {
    public String doSomething(int... args) {
        int sum = IntStream.of(args).sum();
        return String.format("Summen er : %d", sum);
    }
}
