package no.ntnu.imt3281.lambda_streams;

import java.security.SecureRandom;
import java.util.stream.IntStream;

public class EffectiveStreams {

    static long findSumWithFor(int numbers[]) {
        long start = System.currentTimeMillis();
        int sum = 0;
        for (int i=0; i<numbers.length; i++) {
            sum += numbers[i];
        }
        return System.currentTimeMillis()-start;
    }

    static long findSumWithForEach(int numbers[]) {
        long start = System.currentTimeMillis();
        int sum = 0;
        for (int number : numbers) {
            sum += number;
        }
        return System.currentTimeMillis()-start;
    }

    static long findSumWithStreams(int numbers[]) {
        long start = System.currentTimeMillis();
        int sum = IntStream.of(numbers).sum();
        return System.currentTimeMillis()-start;
    }

    static long findSumWithParallelStream(int numbers[]) {
        long start = System.currentTimeMillis();
        int sum = IntStream.of(numbers).parallel().sum();
        return System.currentTimeMillis()-start;
    }

    static long sortStream(int numbers[]) {
        long start = System.nanoTime();
        IntStream.of(numbers).sorted().findFirst();
        return System.nanoTime()-start;
    }

    static long sortParallelStream(int numbers[]) {
        long start = System.nanoTime();
        IntStream.of(numbers).parallel().sorted().findFirst();
        return System.nanoTime()-start;
    }

    public static void main(String[] args) {
        SecureRandom random = new SecureRandom();

        System.out.println("Genererer 60.000.000 tilfeldige tall");
        int randomValues[] = random.ints(60000000, 1, 100000).toArray();
        System.out.printf("Found max value in %d nanosecs with for loop width counter.\n", findSumWithFor(randomValues));
        System.out.printf("Found max value in %d nanosecs with for loop.\n", findSumWithForEach(randomValues));
        System.out.printf("Found max value in %d nanosecs with for streams.\n", findSumWithStreams(randomValues));
        System.out.printf("Found max value in %d nanosecs with for parallel stream.\n", findSumWithParallelStream(randomValues));
        System.out.printf("Sorted numbers in %.2f millisecs\n", sortStream(randomValues)/1000000.);
        System.out.printf("Sorted numbers in parallel in %.2f millisecs", sortParallelStream(randomValues)/1000000.);
    }
}
