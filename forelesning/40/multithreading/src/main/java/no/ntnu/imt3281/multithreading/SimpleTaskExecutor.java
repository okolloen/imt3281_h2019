package no.ntnu.imt3281.multithreading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SimpleTaskExecutor {
    public static void main(String[] args) {
        Sleeper sleepy1 = new Sleeper("Sleepy Joe 1");
        Sleeper sleepy2 = new Sleeper("Sleepy Joe 2");
        Sleeper sleepy3 = new Sleeper("Sleepy Joe 3");

        System.out.println("Starting executor service");
        ExecutorService executorService = Executors.newCachedThreadPool();

        executorService.execute(sleepy1);
        executorService.execute(sleepy2);
        executorService.execute(sleepy3);

        executorService.shutdown();
        System.out.println("Sleeping has started, main ends.");
    }
}
