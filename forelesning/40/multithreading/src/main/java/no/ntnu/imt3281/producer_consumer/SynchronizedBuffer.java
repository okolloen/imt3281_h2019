package no.ntnu.imt3281.producer_consumer;

public class SynchronizedBuffer implements Buffer {
    private int buffer = -1;
    private boolean occupied = false;

    public synchronized void put(int value) throws InterruptedException {
        while (occupied) {
            System.out.println("Producer tries to write");
            displayState("Buffer full, producer waits");
            wait();
        }

        buffer = value;
        occupied = true;

        displayState("Producer writes "+buffer);
        notifyAll();
    }

    public synchronized int get() throws InterruptedException {
        while (!occupied) {
            System.out.println("Consumer tries to read.");
            displayState("Buffer empty, consumer waits.");
            wait();
        }

        occupied = false;
        displayState("Consumer reads "+buffer);
        int value = buffer;
        notifyAll();
        return value;
    }

    private synchronized void displayState(String operation) {
        System.out.printf("%-40s%d\t\t%b\n\n", operation, buffer, occupied);
    }
}
