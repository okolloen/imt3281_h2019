package no.ntnu.imt3281.producer_consumer;

import java.util.concurrent.ArrayBlockingQueue;

public class BlockingBuffer implements Buffer {
    private final ArrayBlockingQueue<Integer> buffer = new ArrayBlockingQueue<Integer>(1);

    @Override
    public void put(int value) throws InterruptedException {
        buffer.put(value);
        System.out.printf("Producer writes\t%2d", value);
    }

    @Override
    public int get() throws InterruptedException {
        int value = buffer.take();
        System.out.printf("Consumer reads\t%2d", value);
        return value;
    }
}
