package no.ntnu.imt3281.producer_consumer;

import java.sql.Time;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class UnsynchronizedBufferTest {
    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newCachedThreadPool();

        Buffer sharedLocation = new UnsynchronizedBuffer();

        System.out.println("Action\t\t\tValue\tSum of produced\tSum of consumed");
        System.out.println("------\t\t\t-----\t---------------\t---------------\n\n");

        executorService.execute(new Producer(sharedLocation));
        executorService.execute(new Consumer(sharedLocation));

        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.MINUTES);
    }
}
