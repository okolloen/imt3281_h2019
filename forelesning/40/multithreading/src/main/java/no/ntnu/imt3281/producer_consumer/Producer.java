package no.ntnu.imt3281.producer_consumer;

import java.security.SecureRandom;

public class Producer implements Runnable {
    private final SecureRandom generator = new SecureRandom();
    private final Buffer sharedLocation;

    public Producer(Buffer sharedLocation) {
        this.sharedLocation = sharedLocation;
    }

    @Override
    public void run() {
        int sum = 0;
        for (int count=0; count<=10;  count++) {
            try {
                Thread.sleep(generator.nextInt(3000));
                sharedLocation.put(count);
                sum += count;
                System.out.printf("\t\t%2d\n", sum);
            } catch (InterruptedException e) {
                e.printStackTrace();
                Thread.currentThread().interrupt();
            }
        }
        System.out.printf("Producer produced values totaling %d\nTerminating producer.\n", sum);
    }
}
