package no.ntnu.imt3281.producer_consumer;

public interface Buffer {
    public void put(int value) throws InterruptedException;
    public int get() throws InterruptedException;
}
