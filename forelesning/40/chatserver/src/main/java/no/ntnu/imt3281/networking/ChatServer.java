package no.ntnu.imt3281.networking;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;

public class ChatServer {
    LinkedList<Client> clients = new LinkedList<>();
    boolean stopping = false;
    ArrayBlockingQueue<Message> messagesToSend = new ArrayBlockingQueue<Message>(100);
    ArrayBlockingQueue<Client> disconnectedClients = new ArrayBlockingQueue<>(1000);

    public ChatServer() {
        startServerThread();
        startListener();
        startSenderThread();
        startRemoveDisconnectedClientsThread();
    }

    private void startServerThread() {
        Thread server = new Thread() {
            public void run () {
                try {
                    ServerSocket server = new ServerSocket(4567);
                    while (!stopping) {
                        Socket s = server.accept();
                        try {
                            Client c = new Client(s);
                            synchronized (clients) {
                                clients.add(c);
                            }
                        } catch (IOException e) {
                            System.err.println("Unable to create client from "+s.getInetAddress().getHostName());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        server.start();
    }

    private void startListener() {
        TimerTask checkActivity = new TimerTask() {
            @Override
            public void run() {
                synchronized (clients) {
                    Iterator<Client> iterator = clients.iterator();
                    while (iterator.hasNext()) {
                        Client c = iterator.next();
                        try {
                            String msg = c.read();
                            if (msg != null) {
                                messagesToSend.add(new Message(msg, c.name));
                            }
                        } catch (IOException e) {   // Exception while reading from client, assume client is lost
                            // Do nothing, this is really not likely to happen
                        }
                    }
                }
            }
        };
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(checkActivity, 50L, 50L);
    }

    private void startSenderThread() {
        Thread sender = new Thread() {
            public void run() {
                while (!stopping) {
                    try {
                        Message msg = messagesToSend.take();
                        synchronized (clients) {
                            Iterator<Client> iterator = clients.iterator();
                            while (iterator.hasNext()) {
                                Client c = iterator.next();
                                if (c.name != msg.name) {
                                    try {
                                        c.send(msg.name + ":" + msg.message);
                                    } catch (IOException e) {   // Exception while sending to client, assume client is lost
                                        synchronized (disconnectedClients) {
                                            if (!disconnectedClients.contains(c)) {
                                                disconnectedClients.add(c);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        sender.setDaemon(true);
        sender.start();
    }

    private void startRemoveDisconnectedClientsThread() {
        Thread removeDisconnectedClientsThread = new Thread() {
            public void run() {
                while (!stopping) {
                    try {
                        Client client = disconnectedClients.take();
                        Message msg = new Message(client.name, "Vanished into thin air");
                        synchronized (clients) {
                            clients.remove(client);
                        }
                        messagesToSend.add(msg);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        removeDisconnectedClientsThread.setDaemon(true);
        removeDisconnectedClientsThread.start();
    }

    public static void main(String[] args) {
        new ChatServer();
    }

    class Client {
        Socket s;
        BufferedWriter bw;
        BufferedReader br;
        String name;

        public Client (Socket s) throws IOException {
            bw = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
            br = new BufferedReader(new InputStreamReader(s.getInputStream()));
            name = br.readLine();
        }

        public String read() throws IOException {
            if (br.ready()) {
                return br.readLine();
            }
            return null;
        }

        public void send(String s) throws IOException {
            bw.write(s);
            bw.newLine();
            bw.flush();
        }

        public void close() throws IOException {
            bw.close();
            br.close();
            s.close();
        }
    }

    private class Message {
        String message, name;
        public Message(String msg, String name) {
            message = msg;
            this.name = name;
        }
    }
}
