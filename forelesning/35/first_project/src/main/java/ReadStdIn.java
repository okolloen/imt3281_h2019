import java.util.Scanner;

public class ReadStdIn {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String tmp = scanner.nextLine();
        while (tmp.length()>0) {
            System.out.println(tmp);
            tmp = scanner.nextLine();
        }
        scanner.close();
    }
}
