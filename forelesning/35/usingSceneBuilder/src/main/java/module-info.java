module no.ntnu.imt3281.sceneBuilder {
    requires javafx.controls;
    requires javafx.fxml;

    opens no.ntnu.imt3281.sceneBuilder to javafx.fxml;
    exports no.ntnu.imt3281.sceneBuilder;
}