# Introduksjon til maven

- first_project - opprette et maven prosjekt, fordeler med dette?
   * pom.xml - konfigurasjonsfilen til maven
   * HelloWorld - Hvordan kjøre programmer
   * ReadStdIn - hvordan kan en pipe en fil til std in? Hvordan bruke debugger.
- packages - Hvordan opprette og bruke pakker, hvorfor pakker?
   * InneIEnPakke - private, public, protected og default, hva er tilgjengelig hvor.
   * Students - Leser studenter fra en CSV fil, skriver ut studenter og oversikt over antall studenter i hver klasse.
      - Student - brukes av Students, inneholder informasjon om en enkelt student
- [Spesifisere Java versjon i maven](https://www.baeldung.com/maven-java-version)

# JavaFX GUI i nyere versjoner av JavaFX

[Getting Started with JavaFX 12](https://openjfx.io/openjfx-docs/#introduction)

Bruk fremgangsmåten under "Modular with Maven" for din editor.

For å få tilgang til Plugins->javafx->javafx:jlink så må versjonen for org.openjfx være minst 0.0.3 (default blir 0.0.1 når en følger "oppskriften").

For at det skal lages launcher slik som beskrevet må en også legge til linjene :

~~~~
<launcher>hellofx</launcher>
<jlinkImageName>hellofx</jlinkImageName>
~~~~

i configuration under org.openjfx. NB, launcheren heter : target/hellofx/bin/hellofx, ikke target/hellofx/bin/launcher.



Legg til .idea, *.iml og target i .gitignore, da kan dere dele prosjektet via git og andre kan jobbe med samme prosjektet uavhengig av hvilken editor de bruker.

I katalogen *hellofx* ligger et prosjekt som er laget med fremgangsmåten over.

# Bruk av SceneBuilder for å "tegne" grensesnitt

- [Last ned og installer SceneBuilder](https://gluonhq.com/products/scene-builder/) evt her [https://www.oracle.com/technetwork/java/javase/downloads/javafxscenebuilder-1x-archive-2199384.html](https://www.oracle.com/technetwork/java/javase/downloads/javafxscenebuilder-1x-archive-2199384.html)
- [Bruk av SceneBuilder i IntelliJ](https://www.jetbrains.com/help/idea/opening-fxml-files-in-javafx-scene-builder.html)
- [Bruk av SceneBuilder i Eclipse](https://docs.oracle.com/javafx/scenebuilder/1/use_java_ides/sb-with-eclipse.htm)

Katalogen usingSceneBuilder inneholder et ferdig prosjekt med komplekse grensesnitt.
