package no.ntnu.imt3281.pakke;

public class Student {
    String givenName, familyName;
    String group;

    public Student (String givenName, String familyName, String group) {
        this.givenName = givenName;
        this.familyName = familyName;
        this.group = group;
    }

    public Student (String csv) {
        String[] parts = csv.split(";");
        String tmp = parts[2];
        String group[] = tmp.split("\\)");
        this.givenName = parts[0];
        this.familyName = parts[1];
        this.group = group[0]+")";
    }

    public String getGivenName() {
        return givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public String getGroup() {
        return group;
    }

    @Override
    public String toString() {
        return givenName+" "+familyName+" - "+group;
    }
}
