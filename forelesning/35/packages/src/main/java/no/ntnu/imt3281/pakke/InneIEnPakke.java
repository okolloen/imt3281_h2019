package no.ntnu.imt3281.pakke;

public class InneIEnPakke {
    public int alleKanSe;
    private int kunMetoderIKlassen;
    int defaultAlleISammePakke;
    protected int kunMetoderIKlassenOgKlasserSomArver;

    public InneIEnPakke() {
        System.out.println("Denne klassen er definert i en pakke");
        System.out.println(getClass().getPackage().getName());
    }

    public static void main(String[] args) {
        new InneIEnPakke();
    }
}
