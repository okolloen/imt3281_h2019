package no.ntnu.imt3281.pakke;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Students {
    List<Student> students = new LinkedList<>();

    public Students() {
        Scanner scanner = new Scanner(System.in);
        String tmp = scanner.nextLine();
        tmp = scanner.nextLine();
        do {
            students.add(new Student(tmp));
            tmp = scanner.nextLine();
        } while (scanner.hasNextLine());
        scanner.close();
        for (Student s : students) {
            System.out.println(s);
        }
        klasseOversikt();
    }

    private void klasseOversikt() {
        HashMap<String, List<Student>> groups = new HashMap<String, List<Student>>();
        for (Student s : students) {
            if (groups.containsKey(s.getGroup())) {
                groups.get(s.getGroup()).add(s);
            } else {
                List<Student> studentsInGroup = new LinkedList<Student>();
                studentsInGroup.add(s);
                groups.put(s.getGroup(), studentsInGroup);
            }
        }

        System.out.printf("==============\nDet er registrert %d studenter i emnet\n", students.size());
        for (String key : groups.keySet()) {
            System.out.printf ("Det er %d studenter fra %s\n", groups.get(key).size(), key);
        }
    }

    public static void main(String[] args) {
        new Students();
    }
}
