## Fremdriftsplan

|  Uke  | Tema                    |
| :---: | ----------------------- |
| 34    | Introduksjon til Java   |
| 35  | JavaFX GUI i nyere versjoner av Java. Introduksjon til Maven. Bruk av SceneBuilder for å "tegne" grensesnitt |
| 36 | Enkel håndtering av hendelser i GUI. Dokumentasjon med JavaDoc, I18N |
| 37 | Arrayer, lister, collections rammeverket. Generiske typer. Exception håndtering, filer |
| 38 | Testing med JUnit. Statisk kodetesting med SonarQube. Logging. Lambda for event håndtering |
| 39 | Mer om JavaFX, kapittel 12, 13 og 22 i læreboka |
| 40 | Nettverk, flertrådsprogrammering, lambda og streams |
| 41 | Databaser |
| 42 | Kravspesifikasjon for Ludo |
| 43 | Kravspesifikasjon for Ludo |
| 44 -46 | Jobbe med Ludo prosjektet (Stand up meetings, kode review) |
| 47 | Fullføre Ludo prosjektet |
