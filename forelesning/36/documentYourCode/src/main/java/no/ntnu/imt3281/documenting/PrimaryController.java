package no.ntnu.imt3281.documenting;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Acts as the controller for primary.fxml.
 */
public class PrimaryController {
    private List<Student> students = new LinkedList<>();

    @FXML
    private TextArea input;

    @FXML
    private TextArea output;

    /**
     * Called when the only button in the app is pressed.
     *
     * @param event information about the button press :-)
     */
    @FXML
    void createStats(ActionEvent event) {
        students.clear();
        for (String tmp : input.getText().split("\n")) {
            students.add(new Student(tmp));
        }
        klasseOversikt();
    }

    /**
     * Called from {@link PrimaryController#createStats(ActionEvent)} when the user requests to get the stats.
     * Uses information in a list of {@link Student} objects to get information about the number of students in the different groups (classes).
     */
    private void klasseOversikt() {
        HashMap<String, List<Student>> groups = new HashMap<String, List<Student>>();
        for (Student s : students) {
            if (groups.containsKey(s.getGroup())) {
                groups.get(s.getGroup()).add(s);
            } else {
                List<Student> studentsInGroup = new LinkedList<Student>();
                studentsInGroup.add(s);
                groups.put(s.getGroup(), studentsInGroup);
            }
        }
        String tmp = String.format("==============\nDet er registrert %d studenter i emnet\n", students.size());
        for (String key : groups.keySet()) {
            tmp += String.format ("Det er %d studenter fra %s\n", groups.get(key).size(), key);
        }
        output.setText(tmp);
    }
}
