package no.ntnu.imt3281.documenting;

/**
 * Used to parse one line (information about a single student) from the CSV file and hold information about this student.
 */
public class Student {
    String givenName, familyName;
    String group;

    /**
     *
     */
    public static String FRAKT = "100.00";

    /**
     * Takes a line from a FSWEB CSV dump and parses it to get information about a single student.
     *
     * @param csv the line to parse
     */
    public Student (String csv) {
        String[] parts = csv.split(";");
        String tmp = parts[2];
        String group[] = tmp.split("\\)");
        this.givenName = parts[0];
        this.familyName = parts[1];
        this.group = group[0]+")";
    }

    /**
     * Gets the given name of the student
     * @return the given name of the student
     */
    public String getGivenName() {
        return givenName;
    }

    /**
     * Gets the family name of the student
     * @return the family name of the student
     */
    public String getFamilyName() {
        return familyName;
    }

    /**
     * Gets the group (class) of the student
     * @return the group (class) the student belongs to
     */
    public String getGroup() {
        return group;
    }

    /**
     * Gets a string representation of this student.
     *
     * @see Object#toString()
     * @return the name and group for this student.
     */
    @Override
    public String toString() {
        return givenName+" "+familyName+" - "+group;
    }
}
