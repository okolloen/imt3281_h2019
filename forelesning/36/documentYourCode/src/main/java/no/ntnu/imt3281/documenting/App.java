package no.ntnu.imt3281.documenting;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * <p>This application can be used to get number of students in each group following a course.</p>
 * <p>The input is a CSV dump from FSWEB, use for instance <a href="https://bitbucket.org/okolloen/imt3281_h2019/src/master/forelesning/35/IMT3281-students-autumn-2019.csv">this example on bitbucket</a>, copy only the lines with actual students.</p>
 *
 * <p>The application looks like this<br>
 * <img src="./doc-files/App.png" alt="Showing screenshot of application"><br>
 * Paste the information into the top portion, press the button and the report will be available in the bottom portion.</p>
 *
 * <p>The {@link PrimaryController} contains the code that does the processing while the {@link Student} class is used to hold parse a line from the CSV file and hold information about each student.</p>
 * @author
 * @since 1.2
 */
public class App extends Application {

    private static Scene scene;

    /**
     * Starts the application.
     *
     * @param stage the window for this application
     * @see javafx.application.Application#start(Stage)
     * @throws IOException if unable to load the fxml file
     */
    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(FXMLLoader.load(getClass().getResource("primary.fxml")));
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Starts the application by calling {@link javafx.application.Application#launch(String...)}.
     *
     * @param args not used, but implementation requires it
     */
    public static void main(String[] args) {
        launch();
    }

}