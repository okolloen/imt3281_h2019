module no.ntnu.imt3281.documenting {
    requires javafx.controls;
    requires javafx.fxml;

    opens no.ntnu.imt3281.documenting to javafx.fxml;
    exports no.ntnu.imt3281.documenting;
}