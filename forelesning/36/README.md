# Enkel håndtering av hendlser i GUI

I katalogen *eventHandling* ligger et eksempel som viser hvordan en kan legge til lytterutiner på knapper, tekstfelter og sjekkbokser via SceneBuilder. Har også med hvordan en via programkode kan legge til liknende lytterutiner på radioknapper.

Viser da hvordan en lager objekter av Interface via anonyme klasser slik som vi var innom første uka.

NB, legger koden for å legge til lytterutiner inn i *initialize* metoden da denne automatisk blir kalt etter at alle komponenter fra FXML filen er opprettet. Dersom vi hadde lagt koden i konstruktoren til kontroller klassen så ville ikke noen av komponentene (knapper, tekstfelter osv) eksistert enda.

# Dokumentasjon via JavaDoc

Kort [Introduksjon til JavaDoc](https://www.baeldung.com/javadoc). [Anbefaling fra Oracle, hvordan er JavaDoc brukt der](https://www.oracle.com/technetwork/articles/javase/index-137868.html).

For å linke til eksisterende dokumentasjon, legg til :

- https://docs.oracle.com/en/java/javase/11/docs/api/
- https://openjfx.io/javadoc/11/

under menyvalget *File/Project structure* i taben med navn *Documentation paths*. 

# I18N

[Internasjonaliseringstracket fra The Java Tutorials (Oracle)](https://docs.oracle.com/javase/tutorial/i18n/index.html)

Katalogen *internationalization* inneholder et eksempel som viser de vanligste måtene å forholde seg til i18n av tekst, datoer, tider, tall, valuta og flertallsformer (det er ingen fisker, det er en fisk, det er x fisker). Eksemplet benytter også internasjonalisering i FXML filen.
