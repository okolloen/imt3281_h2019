module no.ntnu.imt3281.i18n {
    requires javafx.controls;
    requires javafx.fxml;

    opens no.ntnu.imt3281.i18n to javafx.fxml;
    exports no.ntnu.imt3281.i18n;
}