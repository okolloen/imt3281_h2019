package no.ntnu.imt3281.i18n;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;

import java.text.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class PrimaryController {
    private String language = "";
    private String country = "";

    @FXML
    private TextArea textOutput;

    @FXML
    private TextArea textCodeOutput;

    @FXML
    private TextArea dateTimeOuput;

    @FXML
    private TextArea dateTimeCode;

    @FXML
    private TextArea numbersCurrencyOutput;

    @FXML
    private TextArea numbersCurrencyCode;

    @FXML
    private TextArea complexTextOutput;

    @FXML
    private TextArea complexTextCode;

    @FXML
    void setCountryGermany(ActionEvent event) {
        country = "DE";
        updateOutput();
    }

    @FXML
    void setCountryEngland(ActionEvent event) {
        country = "GB";
        updateOutput();
    }

    @FXML
    void setCountryNorway(ActionEvent event) {
        country = "NO";
        updateOutput();
    }

    @FXML
    void setCountryUSA(ActionEvent event) {
        country = "US";
        updateOutput();
    }

    @FXML
    void setDefaultCountry(ActionEvent event) {
        country = "";
        updateOutput();
    }

    @FXML
    void setDefaultLanguage(ActionEvent event) {
        language = "";
        updateOutput();
    }

    @FXML
    void setLanguageEnglish(ActionEvent event) {
        language = "en";
        updateOutput();
    }

    @FXML
    void setLanguageGerman(ActionEvent event) {
        language = "de";
        updateOutput();
    }

    @FXML
    void setLanguageNorwegian(ActionEvent event) {
        language = "no";
        updateOutput();
    }

    /**
     * Called when country or language changes, updates code and output areas
     * in all tabs.
     */
    private void updateOutput() {
        // Sets locale from selected country and language
        Locale currentLocale = null;
        String format = "";
        if (language.equals("") && country.equals("")) {
            currentLocale = Locale.getDefault();
            format = "CodeDefault";
        } else if (!language.equals("") && country.equals("")) {
            currentLocale = new Locale(language);
            format = "CodeLanguage";
        } else {
            currentLocale = new Locale(language, country);
            format = "CodeLanguageCountry";
        }

        // The regular getString from a resource bundle
        ResourceBundle messages = ResourceBundle.getBundle("i18n.Messages", currentLocale);
        textOutput.setText(messages.getString("greetings"));

        // Insert passed values into a template from a resource bundle
        MessageFormat formatter = new MessageFormat(messages.getString("text"+format));
        Object[] params = {language, country, "{", "}"};
        textCodeOutput.setText(formatter.format(params));

        // Formatting date/time using new data/time classes
        LocalDateTime dateTime = LocalDateTime.parse("2019-08-27T15:15:30", DateTimeFormatter.ISO_LOCAL_DATE_TIME);

        DateTimeFormatter dtformat = DateTimeFormatter.ofPattern("MMM d yyyy  hh:mm a");
        String out = dateTime.format(dtformat);
        dateTimeOuput.setText(out);
        dtformat = DateTimeFormatter.ofPattern("MMM d yyyy  HH:mm");
        String out1 = dateTime.format(dtformat);

        // Formatting date/time using the old classes from String
        DateFormat dateFormatter = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, currentLocale);
        Date dateTime1 = Date.from( dateTime.atZone( ZoneId.systemDefault()).toInstant());
        String out2 = dateFormatter.format(dateTime1);

        dateTimeOuput.setText(String.format("%s\n%s\n%s", out, out1, out2));
        formatter = new MessageFormat(messages.getString("dateTime"+format));
        dateTimeCode.setText(formatter.format(params));

        // Formatting numbers, currency,
        int integer = 1234567;
        double decimals = 34567.123;
        double money = 987654.32;

        NumberFormat numberFormatter = NumberFormat.getNumberInstance(currentLocale);
        try {
            Currency currentCurrency = Currency.getInstance(currentLocale);
            NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(currentLocale);

            numbersCurrencyOutput.setText(String.format("Heltall: %s\nFlyttall: %s\nLocale: %s\nValuta: %s\nBeløp: %s", numberFormatter.format(integer), numberFormatter.format(decimals), currentLocale.getDisplayName(), currentCurrency.getDisplayName(), currencyFormatter.format(money)));
        } catch (Exception e) {
            numbersCurrencyOutput.setText("Kan ikke vise valuta kun basert på språk, vennligst angi et land.");
        }
        formatter = new MessageFormat(messages.getString("numbersCurrency"+format));
        numbersCurrencyCode.setText(formatter.format(params));

        // Handlin g formatting with plurals
        MessageFormat messageForm = new MessageFormat("");
        messageForm.setLocale(currentLocale);

        double[] fileLimits = {0,1,2};

        String [] fileStrings = {
                messages.getString("noFiles"),
                messages.getString("oneFile"),
                messages.getString("multipleFiles")
        };

        ChoiceFormat choiceForm = new ChoiceFormat(fileLimits, fileStrings);

        String pattern = messages.getString("pattern");
        Format[] formats = {choiceForm, null, NumberFormat.getInstance()};

        messageForm.applyPattern(pattern);
        messageForm.setFormats(formats);

        Object[] messageArguments = {null, "XDisk", null};

        String tmp = "";
        for (int numFiles = 0; numFiles < 4; numFiles++) {
            messageArguments[0] = numFiles;
            messageArguments[2] = numFiles;
            String result = messageForm.format(messageArguments);
            tmp += result+"\n";
        }
        complexTextOutput.setText(tmp);
        formatter = new MessageFormat(messages.getString("complexText"+format));
        complexTextCode.setText(formatter.format(params));
    }

    @FXML
    public void initialize() {
        updateOutput();
    }
}
