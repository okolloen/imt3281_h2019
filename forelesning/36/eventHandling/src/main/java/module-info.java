module no.ntnu.imt3281.eventHandling {
    requires javafx.controls;
    requires javafx.fxml;

    opens no.ntnu.imt3281.eventHandling to javafx.fxml;
    exports no.ntnu.imt3281.eventHandling;
}