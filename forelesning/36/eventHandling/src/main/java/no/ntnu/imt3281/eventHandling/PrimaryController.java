package no.ntnu.imt3281.eventHandling;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class PrimaryController {
    @FXML private Button button;
    @FXML private CheckBox checkBox1;
    @FXML private CheckBox checkBox2;
    @FXML private CheckBox checkBox3;
    @FXML private RadioButton radioButton1;
    @FXML private RadioButton radioButton2;
    @FXML private RadioButton radioButton3;
    @FXML private RadioButton radioButton4;
    @FXML private TextArea textarea2;
    @FXML private TextField textfield;
    @FXML private Button textFieldButton;
    @FXML private TextArea textAreaOuput;

    @FXML
    void CheckBox1Pressed(ActionEvent event) {
        appendOutput("CheckBox 1 trykket, sjekkboksen er "+(checkBox1.isSelected()?"valgt":"ikke valgt"));
    }

    @FXML
    void CheckBox2Pressed(ActionEvent event) {
        appendOutput("CheckBox 2 trykket, sjekkboksen er "+(((CheckBox)event.getSource()).isSelected()?"valgt":"ikke valgt"));
    }

    @FXML
    void CheckBox3Pressed(ActionEvent event) {
        appendOutput("CheckBox 3 trykket, sjekkboksen er "+(((CheckBox)event.getSource()).isSelected()?"valgt":"ikke valgt"));
    }

    @FXML
    void aboutMenuItemPressed(ActionEvent event) {
        appendOutput("Om dette programmet :-)");
    }

    @FXML
    void buttonPressed(ActionEvent event) {
        textAreaOuput.setText(textAreaOuput.getText()+"Knapp trykket: "+event+"\n");
    }

    @FXML
    void closeMenuItemPressed(ActionEvent event) {
        appendOutput("Lukker\nNot!");
    }

    @FXML
    void deleteMenuItemPressed(ActionEvent event) {
        textAreaOuput.setText("");
    }

    @FXML
    void transfer(ActionEvent event) {
        textarea2.setText(textarea2.getText()+textfield.getText()+"\n");
        textfield.setText("");
    }

    private void appendOutput(String output) {
        textAreaOuput.setText(textAreaOuput.getText()+output+"\n");
    }

    @FXML
    public void initialize() {
        radioButton1.setOnAction(new EventHandler<ActionEvent>() {
            /**
             * @param event
             */
           @Override
           public void handle(ActionEvent event) {
               appendOutput("Radioknapp 1 er "+(radioButton1.isSelected()?"valgt":"ikke valgt"));
           }
        });

        EventHandler handleRadioButtons = new EventHandler() {
            @Override
            public void handle(Event event) {
                if (event.getSource()==radioButton2) {
                    appendOutput("Radioknapp 2 er "+(radioButton2.isSelected()?"valgt":"ikke valgt"));
                } else {
                    appendOutput(((RadioButton)event.getSource()).getText()+" er "+(((RadioButton)event.getSource()).isSelected()?"valgt":"ikke valgt"));
                }
            }
        };

        radioButton2.setOnAction(handleRadioButtons);
        radioButton3.setOnAction(handleRadioButtons);
        radioButton4.setOnAction(handleRadioButtons);
    }


}
