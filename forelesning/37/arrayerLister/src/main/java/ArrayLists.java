import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ArrayLists {
    public static void main(String[] args) {
        String[] colors = {"MAGENTA", "RED", "WHITE", "BLUE", "CYAN"};

        List<String> list = new ArrayList<String>();
        for (String color : colors) {
            list.add(color);
        }

        System.out.println("Content of list using for .....");
        for (int i=0; i<list.size(); i++) {
            System.out.println(list.get(i));
        }

        System.out.println("Content of list using iterator");
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        System.out.println("Removing items with long names from list");
        iterator = list.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().length()>4) {
                iterator.remove();
            }
        }
        for (String item : list) {
            System.out.println(item);
        }
    }
}
