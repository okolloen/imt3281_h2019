/**
 * Objects of this class holds a single card from a deck of card, cards can be compared and
 * it is also possible to check to see if two cards are equal (same face and suit).
 */
public class Card implements Comparable<Card> {
    public static enum Face {Ace, Deuce, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King};
    public static enum Suit {Clubs, Diamonds, Hearts, Spades};

    private final Face face;
    private final Suit suit;

    /**
     * Create a new card with the given face and suit.
     * @param face the face (Ace...King) of the card
     * @param suit the suit of the card (Clubs, Diamonds, Hearts or Spades)
     */
    public Card(Face face, Suit suit) {
        this.face = face;
        this.suit = suit;
    }

    public Face getFace() {
        return face;
    }

    public Suit getSuit() {
        return suit;
    }

    /**
     * Returns the face and suit of the card as "Face of Suit"
     *
     * @return a string representing the card
     */
    @Override
    public String toString() {
        return String.format("%s of %s", face, suit);
    }

    /**
     * Used to compare two cards.
     *
     * @see Comparable#compareTo(Object)
     * @param card the card to compare this card to
     * @return -1 if card is less than this card, 1 if card is larger than this card. Return 0 if the cards are equal.
     */
    @Override
    public int compareTo(Card card) {
        if (suit.compareTo(card.getSuit())==0) {
            return face.compareTo(card.getFace());
        }
        return suit.compareTo(card.getSuit());
    }

    /**
     * Used to check if this card is equal  to a given object
     *
     * @see Object#equals(Object)
     * @param o the object to check this card against
     * @return return true if the object is a card with the same face and suit, otherwise returns false.
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof Card) {
            Card card = (Card)o;
            return (suit==card.getSuit()&&face==card.getFace());
        }
        return false;
    }
}