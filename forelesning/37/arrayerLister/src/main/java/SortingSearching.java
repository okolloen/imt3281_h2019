import java.util.Collections;

public class SortingSearching {
    public static void main(String[] args) {
        DeckOfCards cards = new DeckOfCards();
        System.out.println("Cards before shuffle");
        cards.printCards();
        cards.shuffle();
        System.out.println("\nCards after shuffle");
        cards.printCards();

        cards.sortWithComparator();
        System.out.println("\nCards after sorting with comparator");
        cards.printCards();

        cards.shuffle();
        cards.sort();
        System.out.println("\nCards after sorting without comparator");
        cards.printCards();

        Card card = new Card(Card.Face.Ace, Card.Suit.Spades);
        int pos = cards.find(card);
        System.out.printf("\nThe %s is at position %d in the deck\n", card, pos);

        boolean removed = cards.remove(card);
        if (removed) {
            System.out.printf("\nRemoved the %s from the deck\n", card);
        }
        pos = cards.find(card);
        System.out.printf("The %s is at position %d in the deck", card, pos);

    }
}
