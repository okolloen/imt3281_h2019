import java.util.*;

/**
 * Objects of this class holds a full deck of cards and can perform different operations on the cards.
 *
 */
public class DeckOfCards {
    private List<Card> deckOfCards = new ArrayList<>(52);

    /**
     * Constructor, create a new full deck of cards.
     */
    public DeckOfCards() {
        for (Card.Suit suit : Card.Suit.values()) {
            for (Card.Face face : Card.Face.values()) {
                Card card = new Card(face, suit);
                deckOfCards.add(card);
            }
        }
    }

    /**
     * @see Collections#shuffle(List)
     */
    public void shuffle () {
        Collections.shuffle(deckOfCards);
    }

    /**
     * @see Collections#sort(List, Comparator)
     */
    public void sortWithComparator() {
        Collections.sort(deckOfCards, new CardComparator());
    }

    /**
     * @see Collections#sort(List)
     * @see Comparable
     */
    public void sort() {
        Collections.sort(deckOfCards);                      // Card must implement Comparable
    }

    /**
     * @see Collections#binarySearch(List, Object)
     * @param card the card to find
     * @return the index of the card being searched for, or the index to insert the card at to keep the collection sorted if card not found
     */
    public int find(Card card) {
        return Collections.binarySearch(deckOfCards, card); // Card must implement Comparable
    }

    /**
     * @see List#remove(Object)
     * @see Object#equals(Object)
     * @param card the card to remove
     * @return true if card was removed or false if card not found
     */
    public boolean remove(Card card) {
        return deckOfCards.remove(card);                    // Card must implement equals
    }

    /**
     * Print out all the cards in four columns
     */
    public void printCards() {
        for (int i=0; i<deckOfCards.size(); i++) {
            System.out.printf("%-19s %s", deckOfCards.get(i), (((i+1)%4==0)?"\n":""));
        }
    }
}
