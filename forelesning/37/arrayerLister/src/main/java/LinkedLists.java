import java.util.*;

public class LinkedLists {
    public static void main(String[] args) {
        String[] colors = {"black", "blue", "yellow"};
        LinkedList<String> list = new LinkedList<>(Arrays.asList(colors));

        list.addLast("red");
        list.add("pink");
        list.add(3, "green");
        list.addFirst("cyan");

        System.out.printf ("Items in list: %s\n", list);

        // Change items in the list
        ListIterator<String> iterator = list.listIterator();
        while (iterator.hasNext()) {
            String color = iterator.next();
            iterator.set(color.toUpperCase());
        }
        System.out.printf("Colors in uppercase: %s\n", list);

        // Backwards
        iterator = list.listIterator(list.size());
        System.out.print("Colors backwards: ");
        while (iterator.hasPrevious()) {
            System.out.printf("%s ", iterator.previous());
        }
        System.out.println();

        // Remove items BLACK ... YELLOW
        list.subList(1, 4).clear();
        System.out.printf("Items BLACK...YELLOW removed: %s\n", list);
    }
}
