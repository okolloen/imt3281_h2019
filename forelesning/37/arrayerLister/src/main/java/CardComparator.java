import java.util.Comparator;
import java.util.List;

/**
 * Used by {@link java.util.Collections#sort(List, Comparator)} to sort a deck of cards.
 */
public class CardComparator implements Comparator<Card> {

    /**
     * Takes in two cards, compares them and return -1, 0 or 1.
     * @param card1 the first of the two cards to compare
     * @param card2 the second of the two cards to compare
     * @return -1 if card1 is smaller than card2, 1 if the reverse is true and 0 if they are equal.
     */
    @Override
    public int compare(Card card1, Card card2) {
        if (card1.getSuit().compareTo(card2.getSuit())==0) {
            return card1.getFace().compareTo(card2.getFace());
        }
        return card1.getSuit().compareTo(card2.getSuit());
    }
}
