# Arrayer, lister, Collections rammeverket

I katalogen ArrayerLister

- Autoboxing, Automatisk convertering mellom objekter og native datatyper
- ArrayLists, en liste med en array i bakkant
- LinkedLists, en liste bygget på en linket vanligste
- SortingSearching, ulike måter å sortere og søke på
  * DeckOfCards, inneholder en "kortstokk"
  * Card, inneholder et enkelt kort, brukes av DeckOfCards
  * CardComparator, sammenlikner to kort, brukes for SortingSearching

# Generiske klasser og metoder

I katalogen generics :

- Stack, definisjonen på en stack som kan håndtere objekter av brukerdefinert klasse.
- TestStack, viser bruken av klassen Stack
- GenericTestStack, viser hvordan en kan bruke generics for å lage mer fleksible funksjoner
- Wildcards, bruk wildcards for å gjøre bruken av generics enda mer fleksibel.

# Exception håndtering

Katalogen exceptions inneholder følgende :

- WithoutExceptions, dele på 0 uten exception håndtering.
- WithExceptionHandling, viser hvordan en kan bruke exception håndtering for å håndtere feil.
- UsingMyException, en kan også lage sine egne exceptions
  * Egendefinert exception for eksemplet over.
- TryWithResource, hvordan sørge for at alle ressurser frigjøres når et objekt frigjøres.
  * SequenceProvider, en klasse som kan brukes med Try with resource(s) og som alltid vil rydde opp etter seg (samme funksjonalitet som en destruktor.)

# Filer (I/O)

I katalogen filehandling :

- ReadTextFile, et eksempel som viser hvordan en kan lese en tekstfil.
- ReadFileTryWithResources, som første eksemplet, men bruker try-with-resource
- Serialize, leser fra tekstfil, gjør om til objekter og serialiserer disse objektene til disk
  * Student, klassen for objektene i eksemplet over
- ReadSerializedData, leser inn igjen og skriver til skjerm objektene som ble skrevet til disk i Serialize eksemplet.
- ReadJSONData, leser inn en tekstfil og dekoder JSON data.
- PropertieExample, et eksempel på egenskaper (lese fra/skrive til fil)
