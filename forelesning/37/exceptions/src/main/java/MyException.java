public class MyException extends Exception {    // Extending anything but RuntimeException means the exception MUST be handled
    public MyException(String reason) {
        super(reason);
    }
}
