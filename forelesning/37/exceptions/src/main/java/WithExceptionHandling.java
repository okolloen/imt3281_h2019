public class WithExceptionHandling {
    public static void main(String[] args) {
        try {
            double answer = calculate();
        } catch (ArithmeticException ae) {
            System.out.print("Oooops, math problem, must be fixed.");
            ae.printStackTrace();
        }
    }

    private static double calculate() {
        return calculus();
    }

    private static double calculus() {
        return smart();
    }

    private static double smart() {
        return 42/0;
    }
}
