import java.io.IOException;

public class TryWithResource {
    public static void main(String[] args) throws Exception {
        try (SequenceProvider sp = new SequenceProvider()) {
            int number;
            while ((number=sp.nextValue())<10) {
                System.out.println (number);
            }
            throw new Exception("Fy da");
        } catch (IOException e) {
            System.out.printf("Something bad happened: %s", e.getMessage());
        }
    }
}
