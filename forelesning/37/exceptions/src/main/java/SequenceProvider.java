import java.io.Closeable;
import java.io.IOException;

public class SequenceProvider implements Closeable {
    int next = 0;

    public int nextValue() {
        return next++;
    }

    @Override
    public void close() throws IOException {
        // Close all network, file and DB connections, release bound resources.
        System.out.println ("Everything cleaned up after SequenceProvider");

    }
}
