public class UsingMyException {
    public static void main(String[] args) {
        try {
            throw new MyException("Just because");
        } catch (MyException me) {
            me.printStackTrace();
        }

        try {
            int age = getAge();
        } catch (MyException e) {
            e.printStackTrace();
        }
    }

    public static int  getAge() throws MyException {
        return 1;
    }
}
