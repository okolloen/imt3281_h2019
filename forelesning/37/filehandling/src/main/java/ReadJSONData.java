import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;

public class ReadJSONData {
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(ReadJSONData.class.getResourceAsStream("alleStudier.json")))) {
            String tmp;
            while ((tmp=br.readLine())!=null) {
                sb.append(tmp);
            }
            JSONArray array = new JSONArray(sb.toString()); // Need to know if this is a JSONArray or a JSONObject
            for (Object o : array) {
                JSONObject item = (JSONObject)o;    // Inside the array is JSON objects (known structure)
                String name = item.getString("name");
                JSONArray studieSteder = item.getJSONArray("locations");    // The key "locations" contains an Array
                LinkedList<String> steder = new LinkedList<>();
                for (Object o1 : studieSteder) {
                    JSONObject sted = (JSONObject)o1;   // Inside the array is JSON objects
                    steder.add(sted.getString("campusName"));
                }
                System.out.printf ("%s undervises på følgende steder : %s\n", name, steder);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
