import java.io.*;

public class ReadTextFile {
    public ReadTextFile() {
        try {
            File f = new File("./");
            System.out.println(f.getAbsolutePath());
            System.out.println(getClass().getResource("StudentsInIMT3281.csv"));
            //InputStreamReader reader = new InputStreamReader(getClass().getResource("StudentsInIMT3281.csv").openStream());
            FileReader reader = new FileReader("target/classes/StudentsInIMT3281.csv");
            BufferedReader br = new BufferedReader(reader);
            String tmp;
            while ((tmp=br.readLine())!=null) {
                System.out.println(tmp);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        new ReadTextFile();
    }
}
