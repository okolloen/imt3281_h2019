import java.io.*;

public class ReadSerializedData {
    public static void main(String[] args) {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("Students.obj"))) {
            Object o;
            while ((o=ois.readObject())!=null) {
                System.out.println (o);
            }
        } catch (EOFException e) {
            // No other way to detect EOF :-(
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
