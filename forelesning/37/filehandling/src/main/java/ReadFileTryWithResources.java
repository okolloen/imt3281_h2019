import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ReadFileTryWithResources {
    public ReadFileTryWithResources() {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("StudentsInIMT3281.csv")))) {
            String tmp;
            while ((tmp=br.readLine())!=null) {
                System.out.println(tmp);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new ReadFileTryWithResources();
    }
}
