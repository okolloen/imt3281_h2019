import java.io.*;
import java.util.Locale;
import java.util.Properties;

public class PropertiesExample {
    Properties properties = new Properties();

    public PropertiesExample() throws IOException {
        InputStream is = getClass().getResourceAsStream("settings.properties");
        if (is!=null) {
            properties.load(is);
        } else {
            throw new FileNotFoundException("Properties file not found");
        }
        Locale locale = new Locale(properties.getProperty("language"), properties.getProperty("country"));
        System.out.printf("Hi there %s from %s.\n", properties.get("uname"), locale.getDisplayCountry());

        try (FileWriter fw = new FileWriter("settings.properties")) {
            properties.store(fw, "User settings for application");
        } catch (IOException e) {
            System.err.println ("Unable to write properties.");
        }
    }

    public static void main(String[] args) throws IOException {
        new PropertiesExample();
    }

}
