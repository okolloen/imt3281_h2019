import java.io.*;

public class Serialize {
    public Serialize() {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("StudentsInIMT3281.csv")));
             ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("Students.obj"))) {
            String tmp;
            while ((tmp=br.readLine())!=null) {
                Student student = new Student(tmp);
                oos.writeObject(student);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println ("Students serialized and written to 'Students.obj'");
    }

    public static void main(String[] args) {
        new Serialize();
    }
}
