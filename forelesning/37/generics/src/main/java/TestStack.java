public class TestStack {
    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<Integer>();

        for (int i=0; i<10; i++) {
            testPush (stack, i);
        }

        while (!stack.isEmpty()) {
            testPop(stack);
        }

        stack.push(1);
        String test = stack.pop().toString();
    }

    private static void testPop(Stack<Integer> stack) {
        int value = stack.pop();
        System.out.printf("Popped %d of the stack\n", value);
    }

    private static void testPush(Stack<Integer> stack, int i) {
        System.out.printf ("Pushing %d onto stack\n", i);
        stack.push(i);
    }
}
