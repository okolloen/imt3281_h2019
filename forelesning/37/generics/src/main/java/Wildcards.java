import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Wildcards {
    public static void main(String[] args) {
        Number[] tmp = { 1, 2.3, 4, 5.6, 7, 8.9 };
        List<Number> numbers = new ArrayList<Number>();
        for (Number number: tmp) {
            numbers.add(number);
        }

        double sum = sumOfNumbers(numbers);
        System.out.printf("The sum of the numbers %s is %f\n", numbers, sum);

        Integer[] tmp1 = {1, 2,3, 4, 5, 6, 7, 8, 9 };
        List<Integer> numbers1 = Arrays.asList(tmp1);
        //sum = sumOfNumbers(numbers1);
        sum = sumOfNumbers1(numbers1);
        System.out.printf("The sum of the numbers %s is %f\n", numbers1, sum);
        Double[] tmp2 = { 1., 2.3, 4., 5.6, 7., 8.9 };
        List<Double> numbers2 = Arrays.asList(tmp2);
        sum = sumOfNumbers1(numbers2);
        System.out.printf("The sum of the numbers %s is %f\n", numbers1, sum);
    }

    private static double sumOfNumbers1(List<? extends Number> numbers) {
        double tmp = 0;
        for (Number number: numbers) {
            tmp += number.doubleValue();
        }
        return tmp;
    }

    private static double sumOfNumbers(List<Number> numbers) {
        double tmp = 0;
        for (Number number: numbers) {
            tmp += number.doubleValue();
        }
        return tmp;
    }
}
