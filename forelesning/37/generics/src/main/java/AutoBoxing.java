public class AutoBoxing {
    public static void main(String[] args) {
        Integer[] numbers = new Integer[5];
        numbers[0] = 5;                         // Automatically convert int 5 to Integer
        int first = numbers[0];                 // Automatically convert Integer to int
    }
}