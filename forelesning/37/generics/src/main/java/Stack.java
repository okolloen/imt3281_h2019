import java.util.ArrayList;

public class Stack <T>{
    private final ArrayList<T> elements;

    public Stack() {
        this(10);
    }

    public Stack(int capacity) {
        int initCapacity = capacity>0?capacity:10;
        elements = new ArrayList<T>(10);
    }

    public void push (T value) {
        elements.add(value);
    }

    public T pop () {
        if (elements.isEmpty()) {
            throw new RuntimeException("Stack is empty, cannot pop");
        }
        return elements.remove(elements.size()-1);
    }

    public boolean isEmpty() {
        return elements.isEmpty();
    }
}
