import java.util.Collections;

public class GenericTestStack {
    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<Integer>();

        for (int i=0; i<10; i++) {
            testPush (stack, i);
        }

        while (!stack.isEmpty()) {
            testPop(stack);
        }

        Stack<String> stack1 = new Stack<String>();

        String[] tmp = {"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten"};
        for (String number: tmp) {
            testPush(stack1, number);
        }
        while (!stack1.isEmpty()) {
            testPop(stack1);
        }
        testPush(stack1, "");
    }


    private static <T>void testPop(Stack<T> stack) {
        T value = stack.pop();
        System.out.printf("Popped %s of the stack\n", value);
    }

    private static <T>void testPush(Stack<T> stack, T i) {
        System.out.printf ("Pushing %s onto stack\n", i);
        stack.push(i);
    }
}
