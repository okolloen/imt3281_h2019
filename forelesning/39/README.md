# Mer om JavaFX

Inneholder følgende prosjekter :

- cssforfonts, hvordan styre utseende på tekst og komponenter via CSS
- shapes, tegne 2D former, stilsetting via CSS
- tip_calculator, Observer/Observable via egenskaper
- colorchooser, mer om Observer/Observable og egenskaper
- painter, et enkelt tegneprogram
- coverviewer, en liste og et imageview, egendefinert renderer for elementer i listen
- mediaplayer, hvordan spille av video med JavaFX
- webview, hvordan lage en enkel nettleser i JavaFX
