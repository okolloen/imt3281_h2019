package no.ntnu.imt3281.javafx;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;


public class PrimaryController {

    @FXML private TextField address;
    @FXML WebView webView;
    private WebEngine webEngine;

    @FXML
    void initialize() {
        webEngine = webView.getEngine();
        webEngine.locationProperty().addListener(((observableValue, oldValue, newValue) -> {
            address.setText(newValue);
        }));
    }

    @FXML
    void goPressed(ActionEvent event) {

        webEngine.load(address.getText());
    }

}
