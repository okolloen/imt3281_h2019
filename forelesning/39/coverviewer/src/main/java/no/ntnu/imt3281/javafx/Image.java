package no.ntnu.imt3281.javafx;

public class Image {
    private javafx.scene.image.Image thumbnail;
    private javafx.scene.image.Image image;
    private String title;

    public Image(String imgName, String title) {
        this.title = title;
        thumbnail = new javafx.scene.image.Image(getClass().getResourceAsStream("/images/"+imgName+"_small.jpg"));
        image = new javafx.scene.image.Image(getClass().getResourceAsStream("/images/"+imgName+".jpg"));
    }


    public javafx.scene.image.Image getThumbnail() {
        return thumbnail;
    }

    public javafx.scene.image.Image getImage() {
        return image;
    }

    public String getTitle() {
        return title;
    }

    public String toString() {
        return title;
    }
}
