package no.ntnu.imt3281.javafx;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.util.Callback;

public class CoverViewerController {
    @FXML private ListView<Image> listView;
    @FXML private ImageView imageView;

    private final ObservableList<Image> images = FXCollections.observableArrayList();

    @FXML
    public void initialize() {
        images.add(new Image("sunrise1", "Soloppgang 1"));
        images.add(new Image("sunrise2", "Soloppgang 2"));
        images.add(new Image("sunrise3", "Soloppgang 3"));
        images.add(new Image("sunrise4", "Soloppgang 4"));
        images.add(new Image("sunrise5", "Soloppgang 5"));
        images.add(new Image("sunrise6", "Soloppgang 6"));

        System.err.printf("Size of list: %d", images.size());
        listView.setItems(images);

        listView.getSelectionModel().selectedItemProperty().addListener(((observableValue, oldValue, newValue) -> {
            imageView.setImage(newValue.getImage());
        }));

        listView.setCellFactory(imageListView -> {
            return new ImageTextCell();
        });
    }
}
