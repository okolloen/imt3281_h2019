package no.ntnu.imt3281.javafx;

import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.util.Duration;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

public class PrimaryController {

    @FXML private Button playPauseButton;
    @FXML private MediaView mediaView;

    private MediaPlayer mediaPlayer;
    private boolean playing = false;

    @FXML
    void initialize() {
        URL url = null;
        try {
            url = getClass().getResource("/media/video.mp4").toURI().toURL();
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return;
        }
        Media media = new Media(url.toExternalForm());
        mediaPlayer = new MediaPlayer(media);
        mediaView.setMediaPlayer(mediaPlayer);

        mediaPlayer.setOnEndOfMedia(()->{
            playing = false;
            playPauseButton.setText("Play");
            mediaPlayer.seek(Duration.ZERO);
            mediaPlayer.pause();
        });

        mediaPlayer.setOnError(()->{
            Dialog dialog = new Dialog();
            dialog.setContentText(mediaPlayer.getError().toString());
            dialog.showAndWait();
        });

        mediaPlayer.setOnReady(()->{
            DoubleProperty width = mediaView.fitWidthProperty();
            DoubleProperty height = mediaView.fitHeightProperty();

            width.bind(Bindings.selectDouble(mediaView.sceneProperty(), "width"));
            height.bind(Bindings.selectDouble(mediaView.sceneProperty(), "height"));
            mediaView.setPreserveRatio(true);
        });
    }

    public void playPauseButtonPressed(ActionEvent event) {
        playing = !playing;

        if (playing) {
            playPauseButton.setText("Pause");
            mediaPlayer.play();
        } else {
            playPauseButton.setText("Pause");
            mediaPlayer.pause();
        }
    }

}
