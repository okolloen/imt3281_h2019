package  no.ntnu.imt3281.javafx;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;

import java.math.RoundingMode;
import java.text.NumberFormat;

public class PrimaryController {
    private static final NumberFormat currency = NumberFormat.getCurrencyInstance();
    private static final NumberFormat percent = NumberFormat.getPercentInstance();
    private float tipPercentage;

    @FXML
    private Label tipPercentageLabel;

    @FXML
    private TextField amountTextField;

    @FXML
    private TextField tipTextField;

    @FXML
    private TextField totalTextField;

    @FXML
    private Slider tipPercentageSlider;

    @FXML
    void calculateButtonPressed(ActionEvent event) {
        try {
            float amount = Float.parseFloat(amountTextField.getText());
            float tip = amount * tipPercentage;
            float total = amount + tip;

            tipTextField.setText(currency.format(tip));
            totalTextField.setText(currency.format(total));
        } catch (NumberFormatException e) {
            amountTextField.setText("Enter amount");
            amountTextField.selectAll();
            amountTextField.requestFocus();
        }
    }

    @FXML
    void initialize() {
        currency.setRoundingMode(RoundingMode.HALF_UP);
        tipPercentageSlider.valueProperty().addListener((observableValue, oldValue, newValue) -> {
            tipPercentage = newValue.floatValue()/100.0f;   // Slider is 0-30, percent is 0-1.0.
            tipPercentageLabel.setText(percent.format(tipPercentage));
        });
    }
}

