package no.ntnu.imt3281.database;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Utgangspunkt for swapi-import oppgaven
 */
public class App {
    LinkedBlockingQueue<String> toRead = new LinkedBlockingQueue<>();
    LinkedList<String> found = new LinkedList<>();
    final static String[] categories = {"residents", "films", "species", "vehicles", "starships", "characters", "pilots", "planets"};
    final static String[] types = {"people", "planets", "films", "species", "vehicles", "starships"};
    List<String> typesList = Arrays.asList(types);
    final static int[] typeCount = new int[types.length];
    Connection con = null;
    final int MAXTHREADS = 5;

    App() {
        String dbURL = "jdbc:derby:./swapiDB";
        try {                                                   // Attempt to connect to DB
            con = DriverManager.getConnection(dbURL);
        } catch (SQLException e) {                              // if it failed
            if (e.getMessage().equals("Database './swapiDB' not found.")) {    // Is the database missing
                try {                                           // Attempt to create the DB
                    con = DriverManager.getConnection(dbURL + ";create=true");
                    createTable();
                } catch (SQLException e1) {                     // If creation failed, exit
                    System.err.println("Kunne ikke opprette databasen");
                    System.exit(1);
                }
            } else {                                            // Database exists, but could not connect. Exit.
                System.err.println("Kunne ikke koble til databasen");
                System.exit(1);
            }
        }

        toRead.offer("https://swapi.co/api/films/1/");              // Add starting point
        found.offer("https://swapi.co/api/films/1/");
        ExecutorService executor = Executors.newCachedThreadPool();
        System.out.println("Starting first reader thread");
        executor.execute(new Downloader());                             // Start one thread, read the first file
        while (toRead.size()<MAXTHREADS) {   // Wait for first thread to read the first file so we have more items to read
            try {
                Thread.currentThread().sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("\nFirst item read, starting other reader threads");
        for (int i=0; i<MAXTHREADS; i++) {
            executor.execute(new Downloader());                             // We now have more items, start more threads
        }

        executor.shutdown();
        try {
            executor.awaitTermination(10, TimeUnit.MINUTES);        // Wait for threads to finish
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("\n");                                           // Create report
        for (int i=0; i<types.length; i++) {
            System.out.printf("Fant %d elementer i kategorien '%s'.\n", typeCount[i], types[i]);
        }
    }

    class Downloader implements Runnable {
        public void run() {
            PreparedStatement stmt = null;
            try {
                stmt = con.prepareStatement("INSERT INTO swapi (id, type, nr, json) VALUES (?, ?, ?, ?)");
            } catch (SQLException e) {
                e.printStackTrace();
            }

            do {
                try {
                    String next = toRead.take();
                    System.out.print(".");
                    String jsonString = getURLContent(next);
                    JSONObject json = new JSONObject(jsonString);
                    JSONArray lists[] = new JSONArray[categories.length];
                    for (int i = 0; i < categories.length; i++) {
                        lists[i] = json.has(categories[i]) ? json.getJSONArray(categories[i]) : null;
                    }

                    addToDB(stmt, next, jsonString);

                    findNewItems(lists);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } while (!toRead.isEmpty());
        }

        private String getURLContent(String next) throws IOException {
            URL url = new URL(next);                                        // Get information about item
            URLConnection connection = url.openConnection();                // Need a bit extra
            // Must set user-agent, the java default user agent is denied
            connection.setRequestProperty("User-Agent", "curl/7.8 (i386-redhat-linux-gnu) libcurl 7.8 (OpenSSL 0.9.6b) (ipv6 enabled)");
            // Must set accept to application/json, if not html is returned
            connection.setRequestProperty("Accept", "application/json");
            connection.connect();
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            return br.readLine();
        }

        /**
         * Adds a new item to the database, expects a prepared statement,
         * the url from where the item was retrieved and a string with the JSON
         * content of the item as parameters.
         *
         * Extracts the number and type of resource from the url and stores that information
         * with the actual JSON content in the database.
         *
         * @param stmt a prepared statement used to store information in the database
         * @param url the location from witch the content was retrieved
         * @param jsonString the actual JSON content for the item
         */
        private void addToDB(PreparedStatement stmt, String url, String jsonString) {
            String type = url.substring(21);
            String nr = type.substring(type.indexOf("/") + 1, type.length() - 1);
            type = type.substring(0, type.indexOf("/"));

            try {
                stmt.setString(1, url);
                stmt.setString(2, type);
                stmt.setInt(3, Integer.parseInt(nr));
                stmt.setString(4, jsonString);
                stmt.execute();
                typeCount[typesList.indexOf(type)]++;
            } catch (SQLException e) {
                System.err.printf("Does '%s' exist already??\n", url);
            }
        }

        /**
         * Accepts a JSONArray of JSONArrays where the inner arrays contains items.
         * Checks each item in every array and adds the ones we do not already know about.
         *
         * Adds new items both to the linked list "found" that contains all items found and
         * the LinkedBlockingQueue "toRead" where other threads will pick them up and read and
         * parse the content.
         *
         * @param lists a JSONArray containing JSONArrays for each category (planets, movies, characters etc.)
         */
        private void findNewItems(JSONArray[] lists) {
            for (JSONArray list : lists) {
                if (list != null) {
                    list.forEach(item -> {
                        synchronized (found) {
                            if (!found.contains(item)) {
                                found.offer((String) item);
                                toRead.offer((String) item);
                            }
                        }
                    });
                }
            }
        }
    }

    private void createTable() {
        try {
            Statement stmt = con.createStatement();
            stmt.execute("CREATE TABLE swapi (" +
                    "id varchar(255) NOT NULL," +
                    "type varchar(16) NOT NULL," +
                    "nr int NOT NULL," +
                    "json varchar(32672) NOT NULL)");
            stmt.execute("CREATE INDEX searchIndex on swapi (type, nr)");
            stmt.execute("CREATE UNIQUE INDEX idx on swapi (id)");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        for (int i = 0; i < typeCount.length; i++) {    // Make sure this is cleared, should not be neededs
            typeCount[i] = 0;
        }
        new App();
    }
}
