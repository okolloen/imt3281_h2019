package no.ntnu.imt3281.xml;

import javax.xml.bind.JAXB;
import java.io.*;

public class CreateXML {
    public static void main(String[] args) {
        Students students = new Students();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(CreateXML.class.getResourceAsStream("/studentsInIMT3281.csv")));
             BufferedWriter bw = new BufferedWriter(new FileWriter("students.xml"))) {
            String tmp = br.readLine();     // Drop first line (heading)
            while ((tmp=br.readLine())!=null) {
                Student student = new Student(tmp);
                students.getStudents().add(student);
            }
            JAXB.marshal(students, bw);
            System.out.println("Data converted to XML");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
