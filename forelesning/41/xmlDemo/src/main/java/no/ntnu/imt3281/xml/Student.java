package no.ntnu.imt3281.xml;

import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;

public class Student implements Serializable {
    private String givenName, familyName;
    private String group;

    /**
     * Must have empty constructor to be imported from XML
     */
    public Student() {

    }

    public Student (String givenName, String familyName, String group) {
        this.givenName = givenName;
        this.familyName = familyName;
        this.group = group;
    }

    public Student (String csv) {
        String[] parts = csv.split(";");
        String tmp = parts[2];
        String group[] = tmp.split("\\)");
        this.givenName = parts[0];
        this.familyName = parts[1];
        this.group = group[0]+")";
    }

    @XmlElement
    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    @XmlElement
    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    @XmlElement
    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    @Override
    public String toString() {
        return givenName+" "+familyName+" - "+group;
    }
}

