package no.ntnu.imt3281.xml;

import javax.xml.bind.JAXB;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ReadXML {
    public static void main(String[] args) {
        try (BufferedReader br = Files.newBufferedReader(Paths.get("students.xml"))) {
            Students students = JAXB.unmarshal(br, Students.class);
            System.out.printf("%-20s%-20s%-20s\n", "Given name", "Family name", "Class");

            for (Student student : students.getStudents()) {
                System.out.printf("%-20s%-20s%-20s\n", student.getGivenName(), student.getFamilyName(),
                        student.getGroup());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
