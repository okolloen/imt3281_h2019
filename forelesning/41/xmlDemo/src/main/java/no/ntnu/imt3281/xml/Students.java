package no.ntnu.imt3281.xml;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Students {
    @XmlElement(name="student")
    private List<Student> students = new ArrayList<>();

    public List<Student> getStudents() {
        return students;
    }
}
