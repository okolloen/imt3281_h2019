# Testing med JUnit
mavenSonarTesting

Koding DOJO :

- Skriv en test for en klasse Grade med den statiske funksjonen grade, når den får en verdi mellom 0 og 39 så skal det returneres "F";
- Skriv klassen og funksjonen slik at testen passerer.
- Legg til en test som sjekker at når verdier mellom 40 og 49 sendes inn så returneres "E".
- Skriv koden som gjør at testen passerer.
- Legg til en test som sjekker at når verdier mellom 50 og 59 sendes inn så returneres "D".
- Skriv koden som skal til for at testen passerer.
- Legg til en test som sjekker at når verdier mellom 60 og 79 sendes så returneres "C".
- Skriv koden som skal til for at testen passerer.
- Legg til en test som sjekker at når verdier mellom 80 og 89 sendes inn så returneres "B".
- Skriv koden som skal til for at testen passerer.
- Legg til en test som sjekker at når verdier mellom 90 og 100 sendes inn så returneres "A".
- Skriv koden som skal til for at testen passerer.
- Legg til en test som sjekker at når verdier mindre enn 0 sendes inn så returneres "Error: min=0"
- Skriv koden som skal til for at testen passerer.
- Legg til en test som sjekker at når verdier større enn 100 sendes inn så returneres "Error: max=100"
- Skriv koden som skal til for at testen passerer.
- Legg til en test som sjekker at dersom noe annet enn en int sendes inn så returneres "Error: int values only"
- Skriv koden som skal til for at testen passerer.

# Statisk kodetesting med SonarQube

(SonarLint i IntelliJ)[https://www.youtube.com/watch?v=iEW8mkfnMnQ&list=PLcpOXPDTV31oQay22dc-dGeugMUjjuagT&index=10&t=0s]
(Bruke sonar fra maven)[https://docs.sonarqube.org/latest/analysis/scan/sonarscanner-for-maven/]

# Logging

## simple_logging

Grunnleggende bruk av java.util.logging.Logger, henter Logger objekt og bruker dette for å logge hendelser.

##log_to_file

Logger til fil, bruker XMLFormatter for å formatere log meldinger

##logging_with_configuration_file

Henter informasjon om hvordan loggingen skal utføres fra konfigurasjonsfil. Viser også hvordan en kan logge en Exception (med stack trace).

## Lambda for event håndtering

Introduksjon til Lambda (funksjonell) programmering, katalogen *lambda* inneholder et eksempel som bruker lambda for å håndtere events. (Har også lurt inn enkel parsing av JSON data.)
