package no.ntnu.imt3281.testingexample;

import org.junit.Test;

import static org.junit.Assert.*;

public class MessageBuilderTest {

    @Test
    public void testNameMkyong() {

        MessageBuilder obj = new MessageBuilder();
        assertEquals("Hello mkyong", obj.getMessage("mkyong"));

        assertEquals("Please provide a name!", obj.getMessage(""));
        assertEquals("Please provide a name!", obj.getMessage(null));
    }

}