# simple_logging

Grunnleggende bruk av java.util.logging.Logger, henter Logger objekt og bruker dette for å logge hendelser.

# log_to_file

Logger til fil, bruker XMLFormatter for å formatere log meldinger

# logging_with_configuration_file

Henter informasjon om hvordan loggingen skal utføres fra konfigurasjonsfil. Viser også hvordan en kan logge en Exception (med stack trace).
