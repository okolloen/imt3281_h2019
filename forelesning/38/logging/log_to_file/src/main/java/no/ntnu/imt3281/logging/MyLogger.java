package no.ntnu.imt3281.logging;

import java.util.logging.Logger;

/**
 * Use getLogger from this class to always get the same logger object, to be used throughout a project.
 */
public class MyLogger {
    // This will be initialized the first time the class is accessed.
    private static Logger logg = Logger.getLogger(MyLogger.class.getPackage().toString());

    /**
     * Get the Logger object, this will always return the same object reference.
     *
     * @return the logger object to use for logging
     */
    public static Logger getLogger () {
        return logg;
    }
}
