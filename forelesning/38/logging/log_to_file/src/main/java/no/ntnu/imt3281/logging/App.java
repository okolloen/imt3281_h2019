package no.ntnu.imt3281.logging;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.XMLFormatter;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        // Get logger
        Logger logg = MyLogger.getLogger();
        // Log most everything
        logg.setLevel(Level.FINE);
        logg.setUseParentHandlers(false);        // Stops console log
        // Set up a new file handler
        FileHandler fh = null;
        try {
            fh = new FileHandler("./loggTilFil.log");
            logg.fine("Handler -> MemoryHandler, FileHandler, ConsoleHandler, SocketHandler");
            fh.setLevel(Level.INFO);
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Set up an XML formatter
        XMLFormatter xml = new XMLFormatter();
        logg.fine("Formatter -> SimpleFormatter, XMLFormatter");
        // Set the formatter for the log handler
        fh.setFormatter(xml);
        // add the log handler to the logger
        logg.addHandler(fh);
        // Log some events
        logg.severe("Dette er alvorlig");
        logg.warning("Advarsel");
        logg.info("Til informasjon");
        logg.fine("Bagateller");
        logg.finest("Knapt verdt å nevne");
    }
}
