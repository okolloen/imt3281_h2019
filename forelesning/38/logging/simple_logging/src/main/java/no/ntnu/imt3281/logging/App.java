package no.ntnu.imt3281.logging;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Simple App to test logging in Java.
 *
 */
public class App {
    // Get the logger for App
    private static Logger logg = Logger.getLogger(App.class.getPackage().toString());

    /**
     * Get the Logger object
     *
     * @return the logger object to use for logging
     */
    public static Logger getLogger() {
        return logg;
    }

    public static void main(String args[]) {
        // main har tilgang til logg siden logg er statisk i klassen
        logg.info("Main har startet");
        try {
            throw new Exception("Ny exception er kastet i main");
        } catch (Exception e) {
            logg.warning("En feil ble håndtert i main");
            logg.log(Level.INFO, e.getMessage(), e);
        }
        logg.severe("Avslutter programmet");
    }
}
