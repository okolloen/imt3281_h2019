package no.ntnu.imt3281.lambda;

import javafx.beans.property.Property;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import org.json.JSONArray;

public class PrimaryController {

    @FXML private Button button;
    @FXML private CheckBox checkbox;
    @FXML private ListView<String> list;
    @FXML private Slider slider;
    @FXML private TextArea output;

    @FXML
    void initialize () {
        ObservableList<String> observableList = FXCollections.observableArrayList();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("colors.json")))) {
            String tmp;
            StringBuilder sb = new StringBuilder();
            while ((tmp = br.readLine())!=null) {
                sb.append(tmp);
            }
            JSONArray arr = new JSONArray(sb.toString());
            //for (Object item: arr)
            arr.forEach(item->{
                observableList.add((String)item);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        list.setItems(observableList);

        button.setOnAction(actionEvent -> {
            output.appendText("Knappen er trykket\n");
        });

        checkbox.setOnAction(actionEvent -> {
            output.appendText("Checkboxen er trykket");
        });

        list.getSelectionModel().selectedItemProperty().addListener(e->{
            System.out.println(e);
            output.appendText("Du har valgt fargen:+" +
                    list.getSelectionModel().getSelectedItem()+"\n");
        });

        slider.valueProperty().addListener(e->{
           System.out.println(e);
           output.appendText("Verdien på slideren er : "+slider.getValue()+"\n");
        });
    }
}
