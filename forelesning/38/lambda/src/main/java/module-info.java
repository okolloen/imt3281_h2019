module no.ntnu.imt3281.lambda {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.json;

    opens no.ntnu.imt3281.lambda to javafx.fxml;
    exports no.ntnu.imt3281.lambda;
}