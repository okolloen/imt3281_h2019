public class HeiOgHalloAnonymeKlasser {
    Object heiOgHallo[] = new Object[10];

    public HeiOgHalloAnonymeKlasser() {
        for (int i = 0; i < 10; i++) {
            if (i % 3 == 0) {
                heiOgHallo[i] = new Hei() {
                    public void hei() {
                        System.out.println("hei");
                    }

                    @Override
                    public String toString() {
                        return "Hei";
                    }
                };
            } else if (i % 2 == 0) {
                heiOgHallo[i] = new Hallo() {
                    public void hallo() {
                        System.out.println("Hallo");
                    }

                    @Override
                    public String toString() {
                        return "Hallo";
                    }
                };
            } else {
                heiOgHallo[i] = new JegKanHeiOgHallo() {
                    public String toString() {
                        return "Hei og Hallo";
                    }
                };
            }
        }

        for (Object o : heiOgHallo) {
            if (o instanceof Hallo) {
                ((Hallo) o).hallo();
            }
            if (o instanceof Hei) {
                ((Hei) o).hei();
            }
            System.out.println("-----------");
        }
        for (Object o : heiOgHallo) {
            System.out.println(o);
        }
    }

    public static void main(String[] args) {
        new HeiOgHalloAnonymeKlasser();
    }
}
