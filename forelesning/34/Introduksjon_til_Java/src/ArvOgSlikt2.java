public class ArvOgSlikt2 extends ArvOgSlikt1 {
    public ArvOgSlikt2() {
        System.out.println("Inne i konstruktoren for ArvOgSlikt2");
    }

    public void hallo() {
        System.out.println("Hallo fra ArvOgSlikt2");
    }

    public void halloFraSuper() {
        super.hallo();
    }

    public static void main(String[] args) {
        ArvOgSlikt aos = new ArvOgSlikt();
        aos.hei();

        ArvOgSlikt1 aos1 = new ArvOgSlikt1();
        aos1.hallo();
        aos1.hei();

        ArvOgSlikt2 aos2 = new ArvOgSlikt2();
        aos2.hallo();
        aos2.hei();
        aos2.halloFraSuper();
    }
}
