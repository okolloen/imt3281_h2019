public class HeiOgHallo {
    Object heiOgHallo[] = new Object[10];

    public HeiOgHallo() {
        for (int i=0; i<10; i++) {
            if (i%3==0) {
                heiOgHallo[i] = new JegKanHei();
            } else if (i%2==0) {
                heiOgHallo[i] = new JegKanHallo();
            } else {
                heiOgHallo[i] = new JegKanHeiOgHallo();
            }
        }

        for (Object o : heiOgHallo) {
            if (o instanceof Hallo) {
                ((Hallo) o).hallo();
            }
            if (o instanceof Hei) {
                ((Hei) o).hei();
            }
            System.out.println("-----------");
        }
    }

    public static void main(String[] args) {
        new HeiOgHallo();
    }
}
