public class NoMoreDestructors {
    private String name;
    public NoMoreDestructors(String name) {
        this.name = name;
        System.out.println(("Object "+name+" created."));
    }

    @Override
    public void finalize() {    // Note, deprecated, should not be used (but still works, until suddenly it doesn't)
        System.out.println ("Object "+name+" was removed by garbage collector");
    }

    public static void main(String[] args) throws InterruptedException {    // Must use this because of Thread.sleep()
        NoMoreDestructors nmd = new NoMoreDestructors("first");
        new NoMoreDestructors("second");
        new NoMoreDestructors("third");
        System.gc();                // Clear my garbage
        Thread.sleep(500);
    }
}
