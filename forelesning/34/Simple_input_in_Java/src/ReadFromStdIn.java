import java.util.Scanner;

class ReadFromStdIn {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Hva heter du");
        String name = scanner.nextLine();
        System.out.println("Hei " + name);
        System.out.println("Hvor gammel er du?");
        int age = scanner.nextInt();
        if (age > 40) {
            System.out.println("Du er gammel");
        }
    }
}