import java.util.Scanner;

// Formatet som er forventet er :
// Hver linje inneholder en sirkel eller en firkant, linjer med sirkel starter med "sirkel" og så radius på sirkelen, 
// linjer med firkanter starter med "firkant" og så høyde og bredde. F.eks. slik :
// sirkel 14
// firkant 13 32
// sirkel 42
// slutt
// Avslutt med tom linje
// Dvs at for å avslutt så er det en linje med ordet "slutt" og deretter en tom linje
public class ReadFromPipe {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String tmp = scanner.next();
        do {
            if (tmp.equalsIgnoreCase("sirkel")) {
                int r = scanner.nextInt();
                System.out.println("Sirkel med radius: " + r);
            } else if (tmp.equalsIgnoreCase("firkant")) {
                int h = scanner.nextInt();
                int w = scanner.nextInt();
                System.out.println("Firkant med høyde: " + h + " og bredde: " + w);
            }
            scanner.nextLine(); // Fjerner linjeskiftet
        } while (!(tmp = scanner.next()).equalsIgnoreCase("slutt"));
        scanner.close();
    }
}