# Introduksjon til Java

## Popularitet

- [8 Top Programming Languages & Frameworks of 2019](https://hackernoon.com/8-top-programming-languages-frameworks-of-2019-2f08d2d21a1)
- [TIOBE Index for August 2019](https://www.tiobe.com/tiobe-index/)
- [The 7 Most In-Demand Programming Languages of 2019](https://www.codingdojo.com/blog/the-7-most-in-demand-programming-languages-of-2019)
- [Stack Overflow Developer Survey Results 2019](https://insights.stackoverflow.com/survey/2019#technology-_-programming-scripting-and-markup-languages)

## Første Java Programmer - Introduksjon_til_Java

Bruker Visual Studie Code for disse første eksemplene, går over til IntelliJ neste uke (bedre når vi kan bruke maven for konfigurasjonsstyring)

- HelloWorld - Obligatorisk første program
- BetterHelloWorld - Java er objekt orientert, vi har nettopp laget vårt første objekt
- NoMoreDestructors - Java har ingen destructorer, en garbage collector sørger for å fjerne objekter som ikke lenger brukes.
- ArvOgSlikt2 - Arv i Java fungerer slik dere er vant til. Kan kun arve fra en klasse. ArvOgSlikt2 arver fra arvOgSlikt1 som igjen arver fra ArvOgSlikt. Husk å kalle konstruktor i super (dersom det er ønskelig.)
- HeiOgHallo - Interface benyttes for å definere kun metoder, klasser som implementerer disse Interfacene MÅ da implmentere disse metodene. Filene Hei og Hallo er Interface definisjoner, filene JegKanHei, JegKanHallo og JegKanHeiOgHallo er klasser som implementerer disse interfacene.
- HeiOgHalloAnonyeKlasser - Ofte vil det være definert ferdige interfacer som skal brukes for sortering, søking, GUI event håndtering, flertrådsprogrammering osv uten at en ønsker å lage egne filer med separate klasser for å bruke disse. En kan da lage nye objekter av disse hvor metodedefinisjonene finnes sammen med "new" for å lage objektet. Dette er da anonyme klasser.

## Lese fra konsollet - Simple_input_in_Java

- ReadFromStdin - Leser fra standard input (tastaturet)
- ReadFromPipe - Istedenfor å lese fra tastaturet kan vi la programmet ta input fra en fil som pipes inn. (Kan hende en må kompilere med "javac -encoding UTF-8 ReadFromPipe.java ")
