# Lage en web server med API ved bruk av Spark

"Spark - A micro framework for creating web applications in Kotlin and Java 8 with minimal effort"

Eksemplet bruker Spark for å lage en enkel web server som kan serve statiske sider.

Bruker "routing" for å serve /hello, denne er tilgjengelig via standard get.

/postHere tar i mot data sent som json data (se resources/public/index.html) behandler disse og returnerer json data. Viser hvordan en kan parse og håndtere JSON data.