import org.json.JSONObject;

import static spark.Spark.*;

public class App {
    public static void main(String[] args) {
        staticFiles.location("/public");                // Static files
        get("/hello", (req, res) -> "Hello World");      // Localhost:4567/helle    (with method GET)
        post("/postHere", (req, res) -> {                // localhost:3456/postHere (with method POST)
            JSONObject input = new JSONObject(req.body());      // Data received from client

            JSONObject response = new JSONObject("{}");             // Data to be sent back to client
            response.put("yourname", input.getString("name"));
            response.put("yourEmail", input.getString("email"));

            res.type("application/json");           // Sending json data

            return response.toString();                         // Convert to JSON string
        });
    }
}
